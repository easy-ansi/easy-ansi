import random
from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import cursor as cur
from easyansi.core.color_builder import ColorBuilder
from easyansi.drawing.line_chars import LineChars


class BackgroundLineArt(BaseDemo):

    HEADING = "BackgroundLineArt"
    MIN_COLS = 40
    MIN_ROWS = 15
    # How fast to draw each character, in seconds
    SPEED = 0.001

    def __init__(self):
        super().__init__(BackgroundLineArt.HEADING,
                         BackgroundLineArt.MIN_COLS, BackgroundLineArt.MIN_ROWS,
                         show_heading=False, theme="rgb")

    def run_demo(self):
        prnt(cur.HIDE)
        self.draw_design()
        input()
        prnt(cur.SHOW)

    def draw_design(self):
        # This "algorithm" simulates colors falling down via gravity. The color will prefer straight down first,
        # then try a diagonal down, then as a last resort flow from left to right.

        # Set up colors to use
        cb = ColorBuilder.PALETTE_RGB
        colors = (cb.color("00aa00"), cb.color("00aaaa"), cb.color("aa0000"),
                  cb.color("aa00aa"), cb.color("aa5500"), cb.color("aaaaaa"),
                  cb.color("5555ff"))

        # Draw the screen
        cells = []
        line_chars = LineChars.SINGLE
        fwd_slash = line_chars.diagonal_fwd
        bwd_slash = line_chars.diagonal_bwd
        for row in range(0, self.screen_rows):
            cells.append([])
            for col in range(0, self.screen_cols):
                cells[row].append({})
                # create random slash
                slash_type = random.choice((fwd_slash, bwd_slash))
                cells[row][col]["line_char"] = slash_type
                # start color as random
                color = random.choice(colors)
                if slash_type == fwd_slash:
                    # If the character directly above is bwd_slash
                    if (row - 1 >= 0) and (cells[row - 1][col]["line_char"] == bwd_slash):
                        color = cells[row - 1][col]["color"]
                    # If the character above and to the right is fwd_slash
                    elif ((row - 1 >= 0) and (col + 1 < self.screen_cols) and
                          (cells[row - 1][col + 1]["line_char"] == fwd_slash)):
                        color = cells[row - 1][col + 1]["color"]
                    # If the previous character on the current row is bwd_slash
                    elif (col - 1 >= 0) and (cells[row][col - 1]["line_char"] == bwd_slash):
                        color = cells[row][col - 1]["color"]
                else:  # bwd_slash
                    # If the character directly above is fwd_slash
                    if (row - 1 >= 0) and (cells[row - 1][col]["line_char"] == fwd_slash):
                        color = cells[row - 1][col]["color"]
                    # If the character above and to the left is bwd_slash
                    elif (row - 1 >= 0) and (col - 1 >= 0) and (cells[row - 1][col - 1]["line_char"] == bwd_slash):
                        color = cells[row - 1][col - 1]["color"]
                    # If the previous character on the current row is fwd_slash
                    elif (col - 1 >= 0) and (cells[row][col - 1]["line_char"] == fwd_slash):
                        color = cells[row][col - 1]["color"]
                cells[row][col]["color"] = color
                prnt(color + slash_type)
