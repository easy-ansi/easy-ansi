from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import cursor as cur
from easyansi.core.color_builder import ColorBuilder
from easyansi.core import attributes as a
from easyansi.drawing.line_chars import LineChars
from time import sleep


class EasyANSISplashScreen(BaseDemo):

    HEADING = "Easy ANSI Splash Screen"
    MIN_COLS = 65
    MIN_ROWS = 23
    # How fast to draw each character, in seconds
    SPEED = 0.20

    def __init__(self):
        super().__init__(EasyANSISplashScreen.HEADING,
                         EasyANSISplashScreen.MIN_COLS, EasyANSISplashScreen.MIN_ROWS,
                         theme="256", show_heading=False)

    def run_demo(self):
        prnt(cur.HIDE)
        self.show_message()
        self.draw_border()
        input()
        prnt(cur.SHOW)

    @staticmethod
    def get_next_color_num():
        color = 17
        while True:
            yield color
            color += 1
            if color > 230:
                color = 17

    def draw_border(self):
        old_speed = EasyANSISplashScreen.SPEED
        EasyANSISplashScreen.SPEED = 0.02  # border is long, we want to draw fast
        color_gen = EasyANSISplashScreen.get_next_color_num()
        c256 = ColorBuilder.PALETTE_256
        ch = LineChars.DOUBLE
        prnt(cur.locate(0, 0))
        # top left corner
        text = c256.color(next(color_gen), 0) + ch.top_left
        EasyANSISplashScreen.draw(0, 0, text)
        # top side
        for i in range(1, self.screen_cols - 1):
            text = c256.color(next(color_gen), 0) + ch.horizontal
            EasyANSISplashScreen.draw(0, 0, text)
        # top right corner
        text = c256.color(next(color_gen), 0) + ch.top_right
        EasyANSISplashScreen.draw(0, 0, text)
        # right side
        for i in range(1, self.screen_rows - 1):
            text = c256.color(next(color_gen), 0) + ch.vertical
            # EasyANSISplashScreen.draw(-1, 1, text)
            EasyANSISplashScreen.draw(0, 1, text)
        # bottom right corner
        text = c256.color(next(color_gen), 0) + ch.bottom_right
        # EasyANSISplashScreen.draw(-1, 1, text)
        EasyANSISplashScreen.draw(0, 1, text)
        # bottom side
        text = c256.color(next(color_gen), 0) + ch.horizontal
        EasyANSISplashScreen.draw(-1, 0, text)
        for i in range(1, self.screen_cols - 1):
            text = c256.color(next(color_gen), 0) + ch.horizontal
            EasyANSISplashScreen.draw(-2, 0, text)
        # bottom left corner
        text = c256.color(next(color_gen), 0) + ch.bottom_left
        EasyANSISplashScreen.draw(-2, 0, text)
        # left side
        for i in range(1, self.screen_rows - 1):
            text = c256.color(next(color_gen), 0) + ch.vertical
            EasyANSISplashScreen.draw(-1, -1, text)
        EasyANSISplashScreen.SPEED = old_speed

    def show_message(self):
        # Set initial cursor position
        start_col = max(((self.screen_cols - 63) // 2), 1)
        start_row = max(((self.screen_rows - 21) // 2), 1)
        prnt(cur.locate(start_col, start_row))

        # Set colors
        c256 = ColorBuilder.PALETTE_256
        self.theme["Easy-E"] = c256.color(46, 0)
        self.theme["Easy-a"] = c256.color(82, 0)
        self.theme["Easy-s"] = c256.color(118, 0)
        self.theme["Easy-y"] = c256.color(154, 0)

        self.theme["ANSI-A"] = c256.color(51, 0)
        self.theme["ANSI-N"] = c256.color(87, 0)
        self.theme["ANSI-S"] = c256.color(123, 0)
        self.theme["ANSI-I"] = c256.color(159, 0)

        self.theme["Its-I"] = c256.color(91, 0)
        self.theme["Its-t"] = c256.color(92, 0)
        self.theme["Its-apostrophe"] = c256.color(128, 0)
        self.theme["Its-s"] = c256.color(93, 0)

        self.theme["Not-N"] = c256.color(124, 0)
        self.theme["Not-o"] = c256.color(160, 0)
        self.theme["Not-t"] = c256.color(196, 0)

        self.theme["Hard-H"] = c256.color(130, 0)
        self.theme["Hard-a"] = c256.color(166, 0)
        self.theme["Hard-r"] = c256.color(202, 0)
        self.theme["Hard-d"] = c256.color(208, 0)

        # Draw lettering
        prnt(a.BRIGHT)
        EasyANSISplashScreen.draw(0, 0, EasyANSISplashScreen.build_E(self.theme["Easy-E"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_a(self.theme["Easy-a"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_s(self.theme["Easy-s"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_y(self.theme["Easy-y"]))

        EasyANSISplashScreen.draw(4, -6, EasyANSISplashScreen.build_A(self.theme["ANSI-A"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_N(self.theme["ANSI-N"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_S(self.theme["ANSI-S"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_I(self.theme["ANSI-I"]))

        EasyANSISplashScreen.draw(-52, 4, EasyANSISplashScreen.build_I(self.theme["Its-I"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_t(self.theme["Its-t"]))
        EasyANSISplashScreen.draw(1, -3, EasyANSISplashScreen.build_s(self.theme["Its-s"]))
        EasyANSISplashScreen.draw(-6, -5, EasyANSISplashScreen.build_apostrophe(self.theme["Its-apostrophe"]))

        EasyANSISplashScreen.draw(8, -2, EasyANSISplashScreen.build_N(self.theme["Not-N"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_o(self.theme["Not-o"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_t(self.theme["Not-t"]))

        EasyANSISplashScreen.draw(-35, 1, EasyANSISplashScreen.build_H(self.theme["Hard-H"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_a(self.theme["Hard-a"]))
        EasyANSISplashScreen.draw(0, -3, EasyANSISplashScreen.build_r(self.theme["Hard-r"]))
        EasyANSISplashScreen.draw(0, -5, EasyANSISplashScreen.build_d(self.theme["Hard-d"]))
        prnt(a.NORMAL)

    @staticmethod
    def draw(move_cols, move_rows, text):
        block = ""
        if move_cols != 0 or move_rows != 0:
            block += cur.move(move_cols, move_rows)
        block += text
        prnt(block)
        sleep(EasyANSISplashScreen.SPEED)

    @staticmethod
    def build_A(letter_color):
        letter = letter_color
        next_line = cur.move(-10, 1)
        letter += r"    /\    " + next_line
        letter += r"   /  \   " + next_line
        letter += r"  / /\ \  " + next_line
        letter += r" / ____ \ " + next_line
        letter += "/_/    \\_\\"
        return letter

    @staticmethod
    def build_a(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r"  __ _ " + next_line
        letter += r" / _` |" + next_line
        letter += r"| (_| |" + next_line
        letter += r" \__,_|"
        return letter

    @staticmethod
    def build_d(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r"     _ " + next_line
        letter += r"    | |" + next_line
        letter += r"  __| |" + next_line
        letter += r" / _` |" + next_line
        letter += r"| (_| |" + next_line
        letter += r" \__,_|"
        return letter

    @staticmethod
    def build_E(letter_color):
        letter = letter_color
        next_line = cur.move(-8, 1)
        letter += r" ______ " + next_line
        letter += r"|  ____|" + next_line
        letter += r"| |__   " + next_line
        letter += r"|  __|  " + next_line
        letter += r"| |____ " + next_line
        letter += r"|______|"
        return letter

    @staticmethod
    def build_H(letter_color):
        letter = letter_color
        next_line = cur.move(-8, 1)
        letter += r" _    _ " + next_line
        letter += r"| |  | |" + next_line
        letter += r"| |__| |" + next_line
        letter += r"|  __  |" + next_line
        letter += r"| |  | |" + next_line
        letter += r"|_|  |_|"
        return letter

    @staticmethod
    def build_I(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r" _____ " + next_line
        letter += r"|_   _|" + next_line
        letter += r"  | |  " + next_line
        letter += r"  | |  " + next_line
        letter += r" _| |_ " + next_line
        letter += r"|_____|"
        return letter

    @staticmethod
    def build_N(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r" _   _ " + next_line
        letter += r"| \ | |" + next_line
        letter += r"|  \| |" + next_line
        letter += r"| . ` |" + next_line
        letter += r"| |\  |" + next_line
        letter += r"|_| \_|"
        return letter

    @staticmethod
    def build_o(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r"  ___  " + next_line
        letter += r" / _ \ " + next_line
        letter += r"| (_) |" + next_line
        letter += r" \___/ "
        return letter

    @staticmethod
    def build_r(letter_color):
        letter = letter_color
        next_line = cur.move(-6, 1)
        letter += " _ __ " + next_line
        letter += "| '__|" + next_line
        letter += "| |   " + next_line
        letter += "|_|   "
        return letter

    @staticmethod
    def build_S(letter_color):
        letter = letter_color
        next_line = cur.move(-8, 1)
        letter += r"  _____ " + next_line
        letter += r" / ____|" + next_line
        letter += r"| (___  " + next_line
        letter += r" \___ \ " + next_line
        letter += r" ____) |" + next_line
        letter += r"|_____/ "
        return letter

    @staticmethod
    def build_s(letter_color):
        letter = letter_color
        next_line = cur.move(-5, 1)
        letter += r" ___ " + next_line
        letter += r"/ __|" + next_line
        letter += "\\__ \\" + next_line
        letter += r"|___/"
        return letter

    @staticmethod
    def build_t(letter_color):
        letter = letter_color
        next_line = cur.move(-5, 1)
        letter += r" _   " + next_line
        letter += r"| |  " + next_line
        letter += r"| |_ " + next_line
        letter += r"| __|" + next_line
        letter += r"| |_ " + next_line
        letter += r" \__|"
        return letter

    @staticmethod
    def build_y(letter_color):
        letter = letter_color
        next_line = cur.move(-7, 1)
        letter += r" _   _ " + next_line
        letter += r"| | | |" + next_line
        letter += r"| |_| |" + next_line
        letter += r" \__, |" + next_line
        letter += r"  __/ |" + next_line
        letter += r" |___/ "
        return letter

    @staticmethod
    def build_apostrophe(punctuation_color):
        punctuation = punctuation_color
        next_line = cur.move(-3, 1)
        punctuation += " _ " + next_line
        punctuation += "( )" + next_line
        punctuation += "|/"  # purposely did not add last space here
        return punctuation
