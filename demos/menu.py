from common.Menu import Menu


def start():
    try:
        main_menu()
        finalize()
        print("Easy ANSI demos completed.\n")
    except KeyboardInterrupt:
        finalize()
        print("Easy ANSI demos interrupted.\n")


def finalize():
    """Reset the screen to user defaults."""
    from easyansi.core import screen
    from easyansi.core import colors as c
    from easyansi.core import cursor as cur
    from easyansi.core.utils import prnt
    codes = screen.RESET + c.DEFAULT_FG + c.DEFAULT_BG + screen.CLEAR + cur.HOME + cur.SHOW
    prnt(codes)


def main_menu():
    menu = Menu("Easy ANSI Demos", ["Core Functionality",
                                    "Drawing Demos",
                                    "Fun Stuff",
                                    "Quit"])
    choice = ""
    while choice != "Quit":
        choice = menu.get_choice()
        if choice == "Core Functionality":
            core_functionality_menu()
        elif choice == "Drawing Demos":
            drawing_demos_menu()
        elif choice == "Fun Stuff":
            fun_stuff_menu()


def core_functionality_menu():
    menu = Menu("Easy ANSI Core Functionality Demos", ["16-Color Demo",
                                                       "256-Color Demo",
                                                       "RGB-Color Demo",
                                                       "Attributes Demo",
                                                       "Screen Demo",
                                                       "Cursor / Animation Demo",
                                                       "Ring the Bell",
                                                       "Back to Main Menu"])
    choice = ""
    while choice != "Back to Main Menu":
        choice = menu.get_choice()
        if choice == "16-Color Demo":
            from core_functionality.colors_16_demo import Colors16
            Colors16()
        elif choice == "256-Color Demo":
            from core_functionality.colors_256_demo import Colors256
            Colors256()
        elif choice == "RGB-Color Demo":
            from core_functionality.colors_rgb_demo import ColorsRGB
            ColorsRGB()
        elif choice == "Attributes Demo":
            from core_functionality.attributes_demo import Attributes
            Attributes()
        elif choice == "Screen Demo":
            from core_functionality.screen_demo import Screen
            Screen()
        elif choice == "Cursor / Animation Demo":
            from core_functionality.cursor_demo import Cursor
            Cursor()
        elif choice == "Ring the Bell":
            from core_functionality.screen_demo import ring_the_bell
            ring_the_bell()


def drawing_demos_menu():
    menu = Menu("Easy ANSI Drawing Demos", ["Drawing Characters Chart",
                                            "Horizontal Lines Demo",
                                            "Vertical Lines Demo",
                                            "Shapes Demo",
                                            "Back to Main Menu"])
    choice = ""
    while choice != "Back to Main Menu":
        choice = menu.get_choice()
        if choice == "Drawing Characters Chart":
            from drawing_demos.drawing_chars_chart import DrawingCharsChart
            DrawingCharsChart()
        elif choice == "Horizontal Lines Demo":
            from drawing_demos.horizontal_lines_demo import HorizontalLines
            HorizontalLines()
        elif choice == "Vertical Lines Demo":
            from drawing_demos.vertical_lines_demo import VerticalLines
            VerticalLines()
        elif choice == "Shapes Demo":
            from drawing_demos.shapes_demo import ShapesDemo
            ShapesDemo()


def fun_stuff_menu():
    menu = Menu("Easy ANSI Fun Stuff", ["Easy ANSI Splash Screen",
                                        "Background Line Art",
                                        "Back to Main Menu"])
    choice = ""
    while choice != "Back to Main Menu":
        choice = menu.get_choice()
        if choice == "Easy ANSI Splash Screen":
            from fun_stuff.easy_ansi_splash_screen import EasyANSISplashScreen
            EasyANSISplashScreen()
        elif choice == "Background Line Art":
            from fun_stuff.background_line_art import BackgroundLineArt
            BackgroundLineArt()


if __name__ == "__main__":
    start()
