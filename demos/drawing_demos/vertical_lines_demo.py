from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import colors as c
from easyansi.drawing.lines import Lines
from easyansi.core import cursor as cur

class VerticalLines(BaseDemo):

    HEADING = "Easy ANSI Drawing: Vertical Lines Demo"
    MIN_COLS = 38
    MIN_ROWS = 22

    def __init__(self):
        super().__init__(VerticalLines.HEADING, VerticalLines.MIN_COLS, VerticalLines.MIN_ROWS)

    def run_demo(self):
        self.theme["alt_col"] = c.CYAN_FG + c.BLACK_BG
        scrn = self.build_vertical_columns()
        scrn += cur.locate(0, 21)
        prnt(scrn)
        self.wait_for_enter()

    def build_vertical_columns(self):
        col_data = [("ASCII ", Lines.ASCII, 0),
                    ("BLOCK ", Lines.BLOCK, 1),
                    ("SINGLE", Lines.SINGLE, 2),
                    ("DOUBLE", Lines.DOUBLE, 3)]
        table = ""
        for col_hdg, line_enum, col_num in col_data:
            table += self.build_col(col_hdg, line_enum, col_num)
        return table

    def build_col(self, col_hdg, line_enum, col_num):
        col = cur.locate(col_num * 8, 2)
        col += self.theme["column_heading"] + col_hdg
        col += self.theme["alt_col"] if col_num % 2 == 1 else self.theme["default"]
        col += cur.move(-6, 2)
        # Column Line 1
        col += line_enum.vertical(1)
        col += cur.move(-1, 2)
        col += line_enum.vertical(2)
        col += cur.move(-1, 2)
        col += line_enum.vertical(3)
        col += cur.move(-1, 2)
        col += line_enum.vertical(4)
        # Column Line 2
        col += cur.move(1, -12)
        col += line_enum.vertical(5)
        col += cur.move(-1, 2)
        col += line_enum.vertical(6)
        # Column Line 3
        col += cur.move(1, -11)
        col += line_enum.vertical(7)
        col += cur.move(-1, 2)
        col += line_enum.vertical(8)
        return col
