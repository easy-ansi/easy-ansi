from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import colors as c
from easyansi.drawing.lines import Lines


class HorizontalLines(BaseDemo):

    HEADING = "Easy ANSI Drawing: Horizontal Lines Demo"
    MIN_COLS = 58
    MIN_ROWS = 12

    def __init__(self):
        super().__init__(HorizontalLines.HEADING, HorizontalLines.MIN_COLS, HorizontalLines.MIN_ROWS)

    def run_demo(self):
        self.theme["alt_col"] = c.CYAN_FG + c.BLACK_BG
        scrn = self.newline
        scrn += self.build_horizontal_line_rows()
        prnt(scrn)
        self.wait_for_enter()

    def build_horizontal_line_rows(self):
        row_data = [("ASCII ", Lines.ASCII),
                    ("BLOCK ", Lines.BLOCK),
                    ("SINGLE", Lines.SINGLE),
                    ("DOUBLE", Lines.DOUBLE)]
        table = ""
        for row_label, line_enum in row_data:
            table += self.build_row(row_label, line_enum)
            table += self.newline * 2
        return table

    def build_row(self, row_label, line_enum):
        row = self.theme["row_heading"] + row_label
        for length in range(1, 9):
            row += self.theme["default"] + "  "
            row += self.theme["alt_col"] if length % 2 == 1 else self.theme["default"]
            row += line_enum.horizontal(length)
        return row
