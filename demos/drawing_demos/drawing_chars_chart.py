from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import attributes as a
from easyansi.drawing.line_chars import LineChars
from easyansi.drawing.line_chars import LINE_CHARS_KEYS


class DrawingCharsChart(BaseDemo):

    HEADING = "Easy ANSI Drawing Characters Chart"
    MIN_COLS = 44
    MIN_ROWS = 21

    ROW_HEADINGS = LINE_CHARS_KEYS
    COL_ENUMS = (LineChars.ASCII, LineChars.BLOCK, LineChars.SINGLE, LineChars.DOUBLE)
    COL_SPACING = 2

    def __init__(self):
        super().__init__(DrawingCharsChart.HEADING, DrawingCharsChart.MIN_COLS, DrawingCharsChart.MIN_ROWS)

    def run_demo(self):
        self.calculate_col_widths()
        scrn = self.create_col_headings()
        scrn += self.newline
        scrn += self.create_rows()
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()

    def calculate_col_widths(self):
        """Compute each column width."""
        self.col_widths = []
        self.col_widths.append(len(max(DrawingCharsChart.ROW_HEADINGS, key=len)))
        for i in range(0, len(DrawingCharsChart.COL_ENUMS)):
            col_parts = DrawingCharsChart.COL_ENUMS[i].name.split("_")
            self.col_widths.append(len(max(col_parts, key=len)))

    def create_col_headings(self):
        """Build the column headings."""
        hdg_block = self.theme["column_heading"]
        hdg_block += " " * (self.col_widths[0] + DrawingCharsChart.COL_SPACING)
        # Line 1
        for i in range(1, len(self.col_widths)):
            hdg_block += self.build_col_heading(i, 1)
            if i < len(self.col_widths) - 1:
                hdg_block += " " * DrawingCharsChart.COL_SPACING
        hdg_block += self.newline
        # Line 2
        hdg_block += self.theme["column_heading"]
        hdg_block += " " * (self.col_widths[0] + DrawingCharsChart.COL_SPACING)
        for i in range(1, len(self.col_widths)):
            hdg_block += self.build_col_heading(i, 2)
            if i < len(self.col_widths) - 1:
                hdg_block += " " * DrawingCharsChart.COL_SPACING
        hdg_block += self.newline
        return hdg_block

    def build_col_heading(self, col_num, col_row):
        """Given a column number and line number of the column headings, return the column heading string."""
        col_hdg = None
        col_title_parts = DrawingCharsChart.COL_ENUMS[col_num - 1].name.split("_")
        col_fmt = "{:<" + str(self.col_widths[col_num]) + "}"
        # Line 1
        if col_row == 1:
            if len(col_title_parts) == 2:
                col_hdg = col_fmt.format(col_title_parts[0])
            else:
                col_hdg = col_fmt.format(" ")
        else:  # col_row == 2:
            if len(col_title_parts) == 2:
                col_hdg = col_fmt.format(col_title_parts[1])
            else:
                col_hdg = col_fmt.format(col_title_parts[0])
        return col_hdg

    def create_rows(self):
        """Build the drawing character rows."""
        tbl_rows = ""
        for i in range(0, len(DrawingCharsChart.ROW_HEADINGS)):
            tbl_rows += self.build_row(i)
            tbl_rows += self.newline
        return tbl_rows

    def build_row(self, row_num):
        """Build an individual data row."""
        row = self.theme["row_heading"]
        if row_num % 2 == 0:
            row += a.REVERSE_ON
        fmt = "{:<" + str(self.col_widths[0]) + "}"
        row += fmt.format(DrawingCharsChart.ROW_HEADINGS[row_num])
        row += self.theme["default"]
        for i in range(0, len(DrawingCharsChart.COL_ENUMS)):
            row += " " * DrawingCharsChart.COL_SPACING
            fmt = "{:^" + str(self.col_widths[i + 1]) + "}"
            row += fmt.format(DrawingCharsChart.COL_ENUMS[i].all_chars[DrawingCharsChart.ROW_HEADINGS[row_num]])
        if row_num % 2 == 0:
            row += a.REVERSE_OFF
        return row
