from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import colors as c
from easyansi.drawing.shapes import Shapes
from easyansi.core import cursor as cur


class ShapesDemo(BaseDemo):

    HEADING = "Easy ANSI Drawing: Shapes Demo"
    MIN_COLS = 65
    MIN_ROWS = 20

    def __init__(self):
        super().__init__(ShapesDemo.HEADING, ShapesDemo.MIN_COLS, ShapesDemo.MIN_ROWS)

    def run_demo(self):
        self.theme["box_1"] = c.CYAN_FG + c.BLACK_BG
        self.theme["box_2"] = c.WHITE_FG + c.BLACK_BG
        scrn = self.build_shapes_columns()
        scrn += cur.locate(0, 19)
        prnt(scrn)
        self.wait_for_enter()

    def build_shapes_columns(self):
        col_data = [("ASCII", Shapes.ASCII, 0),
                    ("BLOCK", Shapes.BLOCK, 1),
                    ("SINGLE", Shapes.SINGLE, 2),
                    ("DOUBLE", Shapes.DOUBLE, 3)]
        table = ""
        for col_hdg, shape_enum, col_num in col_data:
            table += self.build_col(col_hdg, shape_enum, col_num)
        return table

    def build_col(self, col_hdg, shape_enum, col_num):
        col = cur.locate(col_num * 17, 2)
        col += self.theme["column_heading"]
        col += col_hdg
        # Box 1
        col += cur.move(-(len(col_hdg)), 2)
        col += self.theme["box_1"]
        col += shape_enum.square(2)
        # Box 2
        col += self.theme["box_2"]
        col += cur.move(1, -1)
        col += shape_enum.rectangle(11, 2)
        # Box 3
        col += self.theme["box_1"]
        col += cur.move(-14, 2)
        col += shape_enum.rectangle(2, 11)
        # Box 4
        col += self.theme["box_2"]
        col += cur.move(1, -10)
        col += shape_enum.square(3)
        # Box 5
        col += self.theme["box_1"]
        col += cur.move(-3, 2)
        col += shape_enum.rectangle(3, 7)
        # Box 6
        col += self.theme["box_1"]
        col += cur.move(1, -10)
        col += shape_enum.rectangle(7, 3)
        # Box 7
        col += self.theme["box_2"]
        col += cur.move(-7, 2)
        col += shape_enum.square(7)
        return col
