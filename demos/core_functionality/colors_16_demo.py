from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import colors as c
from easyansi.core.color_builder import ColorBuilder


class Colors16(BaseDemo):

    HEADING = "Easy ANSI 16-Color Demo"
    MIN_COLS = 46
    MIN_ROWS = 23

    def __init__(self):
        super().__init__(Colors16.HEADING, Colors16.MIN_COLS, Colors16.MIN_ROWS)

    def run_demo(self):
        scrn = self.newline
        scrn += self.build_column_headings()
        scrn += self.build_color_table()
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()

    def build_column_headings(self):
        """Build the column headings."""
        col_hdg = self.theme["column_heading"]
        col_hdg += "Index   FG   BG         Constant      FG   BG "
        col_hdg += self.newline
        col_hdg += self.theme["column_heading_divider"]
        col_hdg += "-----  ---- ----     --------------  ---- ----"
        col_hdg += self.newline
        return col_hdg

    def build_color_table(self):
        """Build the 16-color table."""
        color_table = ""
        # Normal Color Data
        color_table += self.build_color_row("BLACK", c.BLACK_IDX, c.BLACK_FG, c.BLACK_BG, white=True)
        color_table += self.build_color_row("RED", c.RED_IDX, c.RED_FG, c.RED_BG)
        color_table += self.build_color_row("GREEN", c.GREEN_IDX, c.GREEN_FG, c.GREEN_BG)
        color_table += self.build_color_row("YELLOW", c.YELLOW_IDX, c.YELLOW_FG, c.YELLOW_BG)
        color_table += self.build_color_row("BLUE", c.BLUE_IDX, c.BLUE_FG, c.BLUE_BG)
        color_table += self.build_color_row("MAGENTA", c.MAGENTA_IDX, c.MAGENTA_FG, c.MAGENTA_BG)
        color_table += self.build_color_row("CYAN", c.CYAN_IDX, c.CYAN_FG, c.CYAN_BG)
        color_table += self.build_color_row("WHITE", c.WHITE_IDX, c.WHITE_FG, c.WHITE_BG)
        # Bright Color Data
        color_table += self.build_color_row("BRIGHT_BLACK", c.BRIGHT_BLACK_IDX, c.BRIGHT_BLACK_FG, c.BRIGHT_BLACK_BG, white=False)
        color_table += self.build_color_row("BRIGHT_RED", c.BRIGHT_RED_IDX, c.BRIGHT_RED_FG, c.BRIGHT_RED_BG)
        color_table += self.build_color_row("BRIGHT_GREEN", c.BRIGHT_GREEN_IDX, c.BRIGHT_GREEN_FG, c.BRIGHT_GREEN_BG)
        color_table += self.build_color_row("BRIGHT_YELLOW", c.BRIGHT_YELLOW_IDX, c.BRIGHT_YELLOW_FG, c.BRIGHT_YELLOW_BG)
        color_table += self.build_color_row("BRIGHT_BLUE", c.BRIGHT_BLUE_IDX, c.BRIGHT_BLUE_FG, c.BRIGHT_BLUE_BG)
        color_table += self.build_color_row("BRIGHT_MAGENTA", c.BRIGHT_MAGENTA_IDX, c.BRIGHT_MAGENTA_FG, c.BRIGHT_MAGENTA_BG)
        color_table += self.build_color_row("BRIGHT_CYAN", c.BRIGHT_CYAN_IDX, c.BRIGHT_CYAN_FG, c.BRIGHT_CYAN_BG)
        color_table += self.build_color_row("BRIGHT_WHITE", c.BRIGHT_WHITE_IDX, c.BRIGHT_WHITE_FG, c.BRIGHT_WHITE_BG)
        return color_table

    def build_color_row(self, const_name, color_idx, fg_const, bg_const, white=False):
        """Build a table color row."""
        cb = ColorBuilder.PALETTE_16
        ch = "#"
        ch2 = "."
        # Index number column
        row = self.theme["row_heading"]
        row += "{:>3d}".format(color_idx)
        row += " " * 4
        # Foreground by index
        bg_idx = c.BRIGHT_BLACK_IDX if white else c.BLACK_IDX
        row += cb.color(color_idx, bg_idx) + ch * 4
        row += c.WHITE + c.BLACK_BG + " "
        # Background by index
        fg_idx = c.BRIGHT_BLACK_IDX if white else c.BLACK_IDX
        row += cb.color(fg_idx, color_idx) + ch2 * 4
        row += c.WHITE + c.BLACK_BG + " " * 5
        # Constant name
        row += "{:<16}".format(const_name)
        # Foreground by name
        row += fg_const + cb.color(bg=bg_idx) + ch * 4
        row += c.WHITE + c.BLACK_BG + " "
        # Background by name
        row += cb.color(fg_idx) + bg_const + ch2 * 4
        row += self.newline
        return row
