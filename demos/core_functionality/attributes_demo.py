from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import attributes as a


class Attributes(BaseDemo):

    HEADING = "Easy ANSI Attributes Demo"
    MIN_COLS = 46
    MIN_ROWS = 23

    def __init__(self):
        super().__init__(Attributes.HEADING, Attributes.MIN_COLS, Attributes.MIN_ROWS)

    def run_demo(self):
        scrn = self.newline
        scrn += self.build_column_headings()
        scrn += self.build_attribute_table()
        scrn += self.newline
        scrn += self.build_complex_sentence()
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()

    def build_column_headings(self):
        """Build the column headings."""
        col_hdg = self.theme["column_heading"]
        col_hdg += "  Attribute     Normal     Bright       Dim"
        col_hdg += self.newline
        col_hdg += self.theme["column_heading_divider"]
        col_hdg += "-------------  ---------  ---------  ---------"
        col_hdg += self.newline
        return col_hdg

    def build_attribute_table(self):
        block = self.build_attribute_row("NORMAL", a.NORMAL, a.NORMAL)
        block += self.newline
        block += self.build_attribute_row("ITALIC", a.ITALIC_ON, a.ITALIC_OFF)
        block += self.newline
        block += self.build_attribute_row("UNDERLINE", a.UNDERLINE_ON, a.UNDERLINE_OFF)
        block += self.newline
        block += self.build_attribute_row("STRIKETHROUGH", a.STRIKETHROUGH_ON, a.STRIKETHROUGH_OFF)
        block += self.newline
        block += self.build_attribute_row("BLINK", a.BLINK_ON, a.BLINK_OFF)
        block += self.newline
        block += self.build_attribute_row("REVERSE", a.REVERSE_ON, a.REVERSE_OFF)
        block += self.newline
        block += self.build_attribute_row("CONCEAL", a.CONCEAL_ON, a.CONCEAL_OFF)
        return block

    def build_attribute_row(self, attr_desc, attr_on, attr_off):
        """Build an individual attribute line."""
        # Row Heading
        row = self.theme["row_heading"]
        row += "{:<15}".format(attr_desc)
        # Normal Column
        row += self.theme["default"]
        row += attr_on + "Abcd Efgh"
        row += attr_off + "  "
        # Bright Column
        row += attr_on + a.BRIGHT + "Ijkl Mnop"
        row += attr_off + a.NORMAL + "  "
        # Dim Column
        row += attr_on + a.DIM + "Qrst Uvwx"
        row += attr_off + a.NORMAL
        row += self.newline
        return row

    def build_complex_sentence(self):
        """Build a sentence with several attributes."""
        sentence = "This sentence accumulates "
        sentence += a.ITALIC_ON + "italic, "
        sentence += a.UNDERLINE_ON + "underline,"
        sentence += self.newline
        sentence += a.STRIKETHROUGH_ON + "strikethrough, "
        sentence += a.BLINK_ON + "blink, "
        sentence += a.REVERSE_ON + "and reverse"
        sentence += a.ATTRIBUTES_OFF + "."
        sentence += self.newline
        return sentence
