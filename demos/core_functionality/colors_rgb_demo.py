from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core.color_builder import ColorBuilder

cb = ColorBuilder.PALETTE_RGB


class ColorsRGB(BaseDemo):

    HEADING = "Easy ANSI RGB-Color Demo (16,777,216 Colors)"
    MIN_COLS = 74
    MIN_ROWS = 21

    def __init__(self):
        super().__init__(ColorsRGB.HEADING, ColorsRGB.MIN_COLS, ColorsRGB.MIN_ROWS, theme="rgb")

    def run_demo(self):
        self.theme["block_number"] = cb.color("ff8700", "000000")
        # Page 1
        scrn = self.build_color_block("Red", True, False, False)
        scrn += self.newline
        scrn += self.build_color_block("Green", False, True, False)
        scrn += self.newline
        scrn += self.build_color_block("Blue", False, False, True)
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()
        # Page 2
        self.print_screen_heading()
        scrn = self.build_color_block("Red + Green", True, True, False)
        scrn += self.newline
        scrn += self.build_color_block("Red + Blue", True, False, True)
        scrn += self.newline
        scrn += self.build_color_block("Green + Blue", False, True, True)
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()
        # Page 3
        self.print_screen_heading()
        scrn = self.build_color_block("Red + Green + Blue", True, True, True)
        scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()

    def build_color_block(self, block_title, red_flag, green_flag, blue_flag):
        """Build a full color range block."""
        block = ""
        number_format = "{:>3}"
        # Heading for block
        subtitle_intensity = hex(int(255 * 0.75))[2:]
        subtitle_hex = "#"
        subtitle_hex += subtitle_intensity if red_flag else "00"
        subtitle_hex += subtitle_intensity if green_flag else "00"
        subtitle_hex += subtitle_intensity if blue_flag else "00"
        block += cb.color(subtitle_hex, "#000000") + block_title
        block += self.newline
        # Color block
        for i in range(0, 4):
            start_idx = (i * 64)
            end_idx = start_idx + 63
            block += "  " + self.theme["block_number"]
            block += number_format.format(start_idx)
            block += self.theme["default"] + " "
            for j in range(start_idx, end_idx+1):
                bg_color = []
                bg_color.append(j if red_flag else 0)
                bg_color.append(j if green_flag else 0)
                bg_color.append(j if blue_flag else 0)
                block += cb.color("000000", bg_color) + "."
            block += self.theme["default"] + " "
            block += self.theme["block_number"]
            block += number_format.format(end_idx)
            block += self.newline
        return block
