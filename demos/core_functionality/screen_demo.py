from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import screen as s
from easyansi.core import colors as c
from easyansi.core import cursor as cur
from easyansi.core import attributes as a


def ring_the_bell():
    prnt(s.BELL)


class Screen(BaseDemo):

    HEADING = "Easy ANSI Screen Demo"
    MIN_COLS = 45
    MIN_ROWS = 17
    LINE_CHAR = "#"
    CENTER_MARKER = "*"

    def __init__(self):
        super().__init__(Screen.HEADING, Screen.MIN_COLS, Screen.MIN_ROWS)

    def run_demo(self):
        # Extra initialization
        self.left_len = self.center_col - 1
        self.right_len = self.screen_cols - self.center_col
        self.theme["center_marker"] = c.BLACK_FG + c.BRIGHT_YELLOW_BG
        self.theme["line_char"] = self.theme["default"]
        # Run demo
        self.show_initial_screen()
        self.run_instructions_match_sections()
        self.match_sections()
        self.run_instructions_clear_screen_forward()
        self.clear_screen_forward()
        self.run_instructions_clear_screen_backward()
        self.clear_screen_backward()
        self.wait_for_enter()

    def show_initial_screen(self):
        scrn = self.build_top_section()
        scrn += self.newline
        scrn += self.build_section_divider()
        scrn += self.build_bottom_section()
        scrn += self.newline
        prnt(scrn)

    def build_top_section(self):
        """Build the top section."""
        # Line 1
        block = self.theme["line_char"] + Screen.LINE_CHAR * self.left_len
        block += self.theme["center_marker"] + Screen.CENTER_MARKER
        block += self.theme["line_char"] + Screen.LINE_CHAR * self.right_len
        block += self.newline
        # Line 2
        block += self.newline
        # Line 3
        block += self.theme["line_char"] + Screen.LINE_CHAR * self.left_len
        block += self.theme["center_marker"] + Screen.CENTER_MARKER
        block += self.newline
        # Line 4
        block += self.theme["default"] + " " * self.left_len
        block += self.theme["center_marker"] + Screen.CENTER_MARKER
        block += self.theme["line_char"] + Screen.LINE_CHAR * self.right_len
        block += self.newline
        return block

    def build_section_divider(self):
        """Build the screen section divider."""
        line = self.theme["section_divider"] + "-" * self.left_len
        line += self.theme["center_marker"] + Screen.CENTER_MARKER
        line += self.theme["section_divider"] + "-" * self.right_len
        line += self.newline
        return line

    def build_bottom_section(self):
        """Build the bottom section, before applying screen codes."""
        block = ""
        for i in range(0, 4):
            block += self.theme["line_char"] + Screen.LINE_CHAR * self.left_len
            block += self.theme["center_marker"] + Screen.CENTER_MARKER
            block += self.theme["line_char"] + Screen.LINE_CHAR * self.right_len
            block += self.newline
        return block

    def run_instructions_match_sections(self):
        """Show instructions for making the bottom section match the top section."""
        prompt = "Press ENTER to make the bottom section"
        prompt += self.newline + self.theme["press-enter"]
        prompt += "match the top section..."
        self.wait_for_enter(prompt=prompt)

    def match_sections(self):
        """Execute codes to make the bottom section match the top section."""
        block = cur.locate(self.center_col, 9)
        block += s.CLEAR_ROW
        block += cur.locate(self.center_col - 1 + 1, 10)
        block += s.CLEAR_ROW_FWD
        block += cur.locate(self.center_col - 1 - 1, 11)
        block += s.CLEAR_ROW_BWD
        block += cur.locate(0, 13) + s.CLEAR_ROW
        block += cur.locate(0, 14) + s.CLEAR_ROW
        block += cur.locate(0, 13)
        prnt(block)

    def run_instructions_clear_screen_forward(self):
        """Show instructions for clearing the bottom half of the screen."""
        prompt = "Press ENTER to clear the screen from the"
        prompt += self.newline + self.theme["press-enter"]
        prompt += "center of the dashed line to the "
        prompt += a.ITALIC_ON + "BOTTOM" + a.ITALIC_OFF
        prompt += self.newline + self.theme["press-enter"]
        prompt += "of the screen..."
        self.wait_for_enter(prompt=prompt)

    def clear_screen_forward(self):
        block = cur.locate(self.center_col - 1 + 1, 7)
        block += s.CLEAR_SCREEN_FWD
        block += cur.locate(0, 9)
        prnt(block)

    def run_instructions_clear_screen_backward(self):
        """Show instructions for clearing the top half of the screen."""
        prompt = "Press ENTER to clear the screen from the"
        prompt += self.newline + self.theme["press-enter"]
        prompt += "center of the dashed line to the "
        prompt += a.ITALIC_ON + "TOP" + a.ITALIC_OFF
        prompt += self.newline + self.theme["press-enter"]
        prompt += "of the screen..."
        self.wait_for_enter(prompt=prompt)

    def clear_screen_backward(self):
        block = cur.locate(self.center_col - 1 - 1, 7)
        block += s.CLEAR_SCREEN_BWD
        prnt(block)
        self.clear_screen_forward()
        block = cur.locate(0, 9)
        prnt(block)
