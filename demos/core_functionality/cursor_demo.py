from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core import cursor as cur
from easyansi.core import colors as c
from time import sleep


class Cursor(BaseDemo):

    HEADING = "Easy ANSI Cursor Demo"
    MIN_COLS = 56
    MIN_ROWS = 20

    # How fast to draw each character in the flower, in seconds
    SPEED = 0.10

    # What is the left-edge column of the flower
    FLOWER_LEFT_COL = 37
    FLOWER_WIDTH = 15

    def __init__(self):
        super().__init__(Cursor.HEADING, Cursor.MIN_COLS, Cursor.MIN_ROWS)

    def run_demo(self):
        self.show_next_and_prev_line()
        self.show_position_check()
        self.show_art_credits()
        self.draw_flower()
        prnt(cur.locate(0, 19))
        self.wait_for_enter()

    def show_next_and_prev_line(self):
        """Test next_line and prev_line functionality."""
        block = self.theme["sub_heading"]
        block += "You should see 3 text lines below:"
        block += self.theme["default"]
        block += cur.locate(0, 4)
        block += "  This is text line 2."
        block += cur.previous_line(1)
        block += "  This is text line 1."
        block += cur.next_line(2)
        block += "  This is text line 3."
        block += self.newline
        prnt(block)

    def show_position_check(self):
        expected_col, expected_row = 30, 1
        prnt(cur.locate(expected_col, expected_row))
        cur_col, cur_row = cur.position()
        block = cur.locate(0, 7)
        block += self.theme["sub_heading"]
        block += "Cursor Position Test:" + self.newline
        block += f"  Expected: ({expected_col}, {expected_row})" + self.newline
        block += f"  Actual:   ({cur_col}, {cur_row})" + self.newline
        prnt(block)

    def show_art_credits(self):
        """Give credit for the art on the screen."""
        block = self.newline
        block += self.theme["sub_heading"] + "Flower Art By:"
        block += self.theme["default"]
        block += cur.down(1) + cur.locate_col(2) + "Joan G. Stark"
        block += cur.down(2) + cur.locate_col(0)
        block += self.theme["sub_heading"] + "URL:"
        block += self.theme["default"]
        block += cur.down(1) + cur.locate_col(2)
        block += "https://www.asciiart.eu/plants/other"
        block += self.newline
        prnt(block)

    @staticmethod
    def draw(move_cols, move_rows, text):
        block = ""
        if move_cols != 0 or move_rows != 0:
            block += cur.move(move_cols, move_rows)
        block += text
        prnt(block)
        sleep(Cursor.SPEED)

    def draw_flower(self):
        """Draw flower ASCII-art in an animated fashion."""
        self.theme["grass"] = c.GREEN_FG + c.BLACK_BG
        self.theme["flower_stem"] = c.BRIGHT_BLACK_FG + c.BLACK_BG
        self.theme["flower_leaf"] = c.BRIGHT_GREEN_FG + c.BLACK_BG
        self.theme["flower_leaf_filler"] = c.GREEN_FG + c.BLACK_BG
        self.theme["flower_petal"] = c.BRIGHT_WHITE_FG + c.BLACK_BG
        self.theme["flower_center"] = c.BRIGHT_YELLOW_FG + c.BLACK_BG
        self.theme["flower_center_filler"] = c.BRIGHT_YELLOW_FG + c.BLACK_BG
        prnt(cur.HIDE)
        self.draw_flower_grass()
        self.draw_flower_stem()
        self.draw_flower_leaf_1()
        self.draw_flower_leaf_2()
        self.draw_flower_leaf_3()
        self.draw_flower_petals()
        self.draw_flower_center()
        prnt(cur.SHOW)

    def draw_flower_grass(self):
        grass_len = Cursor.FLOWER_WIDTH + 10
        block = cur.locate(Cursor.FLOWER_LEFT_COL - 1 - 5, 17)
        block += self.theme["grass"]
        prnt(block)
        for i in range(0, grass_len):
            Cursor.draw(0, 0, "^")

    def draw_flower_stem(self):
        prnt(self.theme["flower_stem"])
        # Left edge
        Cursor.draw(-15, -1, "|")
        Cursor.draw(0, -1, "/")
        Cursor.draw(0, -1, "/")
        Cursor.draw(-1, -1, "|")
        Cursor.draw(-1, -1, "|")
        Cursor.draw(-1, -1, "|")
        Cursor.draw(-1, -1, "|")
        Cursor.draw(-2, -1, "\\")
        # Right edge
        Cursor.draw(0, 0, "\\")
        Cursor.draw(0, 1, "|")
        Cursor.draw(-1, 1, "|")
        Cursor.draw(-1, 1, "|")
        Cursor.draw(-1, 1, "|")
        Cursor.draw(-1, 1, "/")
        Cursor.draw(-2, 1, "/")
        Cursor.draw(-2, 1, "|")

    def draw_flower_leaf_1(self):
        # Top edge of leaf
        prnt(self.theme["flower_leaf"])
        Cursor.draw(1, -1, "_")
        Cursor.draw(0, 0, "/")
        Cursor.draw(0, -1, "_")
        Cursor.draw(0, 0, "/")
        Cursor.draw(0, -1, "_")
        Cursor.draw(0, 0, "/")
        Cursor.draw(0, -1, ",")
        # Bottom edge of leaf
        Cursor.draw(-1, 1, "|")
        Cursor.draw(-1, 1, "/")
        Cursor.draw(-2, 1, "'")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, ",")
        Cursor.draw(-2, 1, "`")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, "'")
        # Leaf filler
        prnt(self.theme["flower_leaf_filler"])
        Cursor.draw(4, -2, ".")
        Cursor.draw(0, 0, "'")

    def draw_flower_leaf_2(self):
        # Bottom edge of leaf
        prnt(self.theme["flower_leaf"])
        Cursor.draw(-7, -1, ";")
        Cursor.draw(-2, 0, "`")
        Cursor.draw(-2, -1, ",")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, "'")
        Cursor.draw(-2, -1, "\\")
        Cursor.draw(-1, -1, "|")
        Cursor.draw(-1, -1, ",")
        # Top edge of leaf
        Cursor.draw(0, 1, "\\")
        Cursor.draw(0, 0, "_")
        Cursor.draw(0, 1, "\\")
        Cursor.draw(0, 0, "_")
        Cursor.draw(0, 1, "\\")
        # Leaf filler
        prnt(self.theme["flower_leaf_filler"])
        Cursor.draw(-4, -1, ".")
        Cursor.draw(-2, 0, "'")

    def draw_flower_leaf_3(self):
        # Top edge of leaf
        prnt(self.theme["flower_leaf"])
        Cursor.draw(6, -1, "/")
        Cursor.draw(-1, -1, "(")
        Cursor.draw(0, -1, ".")
        Cursor.draw(0, 0, "-")
        Cursor.draw(0, 0, ",")
        # Bottom edge of leaf
        Cursor.draw(0, 1, ")")
        Cursor.draw(-2, 1, "`")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, ".")
        Cursor.draw(-2, 1, "`")
        Cursor.draw(-2, 0, ";")
        # Leaf filler
        prnt(self.theme["flower_leaf_filler"])
        Cursor.draw(1, -2, "-")
        Cursor.draw(0, 0, ".")

    def draw_flower_petals(self):
        # Petal 1
        prnt(self.theme["flower_petal"])
        Cursor.draw(-5, -2, "\\")
        Cursor.draw(-1, 1, "/")
        Cursor.draw(-2, 0, "_")
        Cursor.draw(-2, 0, "_")
        Cursor.draw(-2, 0, "\\")
        Cursor.draw(-1, -1, "/")
        # Petal 2
        Cursor.draw(-2, 1, "-")
        Cursor.draw(-2, 0, "'")
        Cursor.draw(-2, -1, "(")
        Cursor.draw(0, -1, "_")
        Cursor.draw(0, 0, ".")
        # Petal 3
        Cursor.draw(-4, 0, "\\")
        Cursor.draw(-1, -1, "/")
        Cursor.draw(0, 0, "`")
        Cursor.draw(0, 0, "-")
        Cursor.draw(0, 0, ".")
        # Petal 4
        Cursor.draw(-3, -1, "(")
        Cursor.draw(0, -1, ".")
        Cursor.draw(0, 0, "-")
        # Petal 5
        Cursor.draw(0, 1, "\\")
        Cursor.draw(-1, -1, "/")
        Cursor.draw(0, -1, "_")
        Cursor.draw(0, 0, "_")
        Cursor.draw(0, 1, "\\")
        Cursor.draw(-1, 1, "/")
        # Petal 6
        Cursor.draw(0, -1, "-")
        Cursor.draw(0, 0, ".")
        Cursor.draw(0, 1, ")")
        Cursor.draw(-1, 1, "`")
        Cursor.draw(-2, 0, "-")
        Cursor.draw(-2, 0, ".")
        # Petal 7
        Cursor.draw(2, 0, "\\")
        Cursor.draw(-1, 1, "/")
        Cursor.draw(-3, 0, "_")
        Cursor.draw(-2, 0, ".")
        # Petal 8
        Cursor.draw(1, 1, ")")
        Cursor.draw(-2, 1, "'")
        Cursor.draw(-2, 0, "-")

    def draw_flower_center(self):
        # Center outline
        prnt(self.theme["flower_center"])
        Cursor.draw(-2, -2, "/")
        Cursor.draw(-1, -1, "\\")
        Cursor.draw(-2, -1, "_")
        Cursor.draw(-2, 0, "_")
        Cursor.draw(-2, 1, "/")
        Cursor.draw(-1, 1, "\\")
        prnt(self.theme["flower_center_filler"])
        Cursor.draw(0, 0, ";")
        Cursor.draw(0, 0, ";")
        Cursor.draw(-1, -1, ";")
        Cursor.draw(-2, 0, ";")
