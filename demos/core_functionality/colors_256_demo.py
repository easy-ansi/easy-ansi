from common.BaseDemo import BaseDemo
from easyansi.core.utils import prnt
from easyansi.core.color_builder import ColorBuilder

cb = ColorBuilder.PALETTE_256


class Colors256(BaseDemo):

    HEADING = "Easy ANSI 256-Color Demo"
    MIN_COLS = 76
    MIN_ROWS = 24

    BRIGHT_FONT_INDEXES = [0, 4, 8]
    FONT_DEFAULT_IDX = 16
    FONT_BRIGHT_IDX = 231

    def __init__(self):
        super().__init__(Colors256.HEADING, Colors256.MIN_COLS, Colors256.MIN_ROWS, theme="256")

    def run_demo(self):
        scrn = self.build_first_16_colors()
        scrn += self.newline
        scrn += self.build_next_216_colors()
        scrn += self.newline
        scrn += self.build_last_24_colors()
        if self.screen_rows > Colors256.MIN_ROWS:
            scrn += self.newline
        prnt(scrn)
        self.wait_for_enter()

    def build_first_16_colors(self):
        """Show the first 16 standard colors"""
        block = self.theme["sub_heading"] + "First 16 Standard Colors"
        block += self.newline
        block += self.build_color_cell_line(0, 15, 8, 4)
        return block

    def build_next_216_colors(self):
        """Show the next 216 colors"""
        block = self.theme["sub_heading"] + "Next 216 Colors (6x6x6 Cube)"
        block += self.newline
        for i in range(0, 3):
            min_idx = (i * 36) + 16
            max_idx = min_idx + 17
            block += self.build_color_cell_line(min_idx, max_idx, 6, 4, bright_font=True)
        for i in range(3, 6):
            min_idx = (i * 36) + 16
            max_idx = min_idx + 17
            block += self.build_color_cell_line(min_idx, max_idx, 6, 4, bright_font=False)
        block += self.newline
        for i in range(0, 6):
            min_idx = (i * 36) + 34
            max_idx = min_idx + 17
            block += self.build_color_cell_line(min_idx, max_idx, 6, 4)
        return block

    def build_last_24_colors(self):
        """Show the last 24 grayscale colors"""
        block = self.theme["sub_heading"] + "Last 24 Grayscale Colors"
        block += self.newline
        block += self.build_color_cell_line(232, 243, 12, 4, bright_font=True)
        block += self.build_color_cell_line(244, 255, 12, 4)
        return block

    def build_color_cell_line(self, min_idx, max_idx, space_mod, cell_width, bright_font=False):
        line = " "
        counter = 0
        for i in range(min_idx, max_idx + 1):
            if counter % space_mod == 0:
                line += self.theme["default"] + " "
            font_color_idx = Colors256.FONT_BRIGHT_IDX if (bright_font or i in Colors256.BRIGHT_FONT_INDEXES) else Colors256.FONT_DEFAULT_IDX
            line += cb.color(font_color_idx, i)
            cell_format = "{:>" + str(cell_width) + "d}"
            line += cell_format.format(i)
            counter += 1
        line += self.newline
        return line
