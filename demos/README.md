# Easy ANSI Demos

These are small programs that demonstrate the capabilities of Easy ANSI.  Depending on your terminal settings, the
16-color palette screens may look a little different.

To run these demos:
* Download this repository
* Change to the appropriate directory
* Make sure Easy ANSI is installed
* Type 'python menu.py'
  * Depending on your installation, you may need to type 'python3 menu.py'

## Core Functionality

| Program | Description | Screen Shots |
| --- | --- | --- |
| colors_16_demo.py | Show the standard 16-color palette. | [Page 1](./images/colors-16_demo.png) |
| colors_256_demo.py | Show the standard 256-color palette. | [Page 1](./images/colors-256_demo.png) |
| colors_rgb_demo.py | Show the capabilities of the RGB-color palette. | [Page 1 / 3](./images/colors-rgb_demo-page-1.png)<br />[Page 2 / 3](./images/colors-rgb_demo-page-2.png)<br />[Page 3 / 3](./images/colors-rgb_demo-page-3.png) |
| attributes_demo.py | Show the various text attributes. | [Page 1](./images/attributes_demo.png) |
| cursor_demo.py | Tests the functions in the cursor module.<br />This screen draws a picture with animation. | [Page 1](./images/cursor_demo.png) |
| screen_demo.py | Tests the functions in the screen module. | [Page 1 / 4](./images/screen_demo-page-1.png)<br />[Page 2 / 4](./images/screen_demo-page-2.png)<br />[Page 3 / 4](./images/screen_demo-page-3.png)<br />[Page 4 / 4](./images/screen_demo-page-4.png) |

## Drawing Functionality

| Program | Description | Screen Shots |
| --- | --- | --- |
| drawing_chars_chart.py | Show the pre-defined line drawing characters in a chart. | [Page 1](./images/drawing_chars_chart.png) |
| horizontal_lines_demo.py | Show horizontal line drawings. | [Page 1](./images/horizontal_lines_demo.png) |
| shapes_demo.py | Show the shapes that can be drawn. | [Page 1](./images/shapes_demo.png) |
| vertical_lines_demo.py | Show vertical line drawings. | [Page 1](./images/vertical_lines_demo.png) |

## Fun Stuff

| Program | Description | Screen Shots |
| --- | --- | --- |
| background_line_art.py | Draws a screen of random slashes, with random colors, with a simplistic gravity-type algorithm. | [Page 1](./images/background_line_art.png) |
| easy_ansi_splash_screen.py | Shows off drawing characters made with line characters along with a colorful border. And to add to the fun it is animated. | [Page 1](./images/easy_ansi_splash_screen.png) |
