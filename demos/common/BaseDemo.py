import sys
from abc import ABC, abstractmethod
from easyansi.core import screen
from easyansi.core import colors as c
from easyansi.core import cursor as cur
from easyansi.core.utils import prnt
from easyansi.core.color_builder import ColorBuilder

cb256 = ColorBuilder.PALETTE_256
cbrgb = ColorBuilder.PALETTE_RGB


class BaseDemo(ABC):

    THEME_16 = {"default": c.WHITE_FG + c.BLACK_BG,
                "screen_heading": c.BRIGHT_YELLOW_FG + c.BLACK_BG,
                "screen_heading_divider": c.BRIGHT_WHITE_FG + c.BLACK_BG,
                "sub_heading": c.BRIGHT_CYAN_FG + c.BLACK_BG,
                "section_divider": c.CYAN_FG + c.BLACK_BG,
                "press-enter": c.BLACK_FG + c.BRIGHT_GREEN_BG,
                "reset": c.DEFAULT_FG + c.DEFAULT_BG,
                "column_heading": c.BRIGHT_GREEN_FG + c.BLACK_BG,
                "column_heading_divider": c.BRIGHT_CYAN_FG + c.BLACK_BG,
                "row_heading": c.BRIGHT_MAGENTA_FG + c.BLACK_BG}

    THEME_256 = {"default": cb256.color(251, 16),
                 "screen_heading": cb256.color(226, 16),
                 "screen_heading_divider": cb256.color(231, 16),
                 "sub_heading": cb256.color(51, 16),
                 "press-enter": cb256.color(16, 46),
                 "reset": cb256.color(-1, -1)}

    THEME_RGB = {"default": cbrgb.color("c6c6c6", "000000"),
                 "screen_heading": cbrgb.color("ffff00", "000000"),
                 "screen_heading_divider": cbrgb.color("ffffff", "000000"),
                 "press-enter": cbrgb.color("000000", "00ff00"),
                 "reset": cbrgb.color("-1", "-1")}

    def __init__(self, heading, min_cols, min_rows, theme="16", show_heading=True):
        self.heading = heading
        self.min_cols = min_cols
        self.min_rows = min_rows
        self.show_heading = show_heading
        if theme == "16":
            self.theme = BaseDemo.THEME_16
        elif theme == "256":
            self.theme = BaseDemo.THEME_256
        elif theme == "rgb":
            self.theme = BaseDemo.THEME_RGB
        else:
            self.theme = None
        self.sufficient_size, self.screen_cols, self.screen_rows = screen.sufficient_size(self.min_cols, self.min_rows)
        self.center_col = self.screen_cols // 2
        if self.screen_cols % 2 == 1:
            self.center_col += 1
        if not self.sufficient_size:
            self.screen_too_small()
        else:
            self.print_screen_heading()
            self.run_demo()
        BaseDemo.finalize()

    @property
    def newline(self):
        return self.theme["default"] + '\n'

    def screen_too_small(self):
        """The screen is too small, show a message."""
        print("")
        print("ERROR: Your screen size is too small.")
        print("")
        print("Your screen size is reported as:")
        print(f"  {self.screen_cols} columns x {self.screen_rows} rows")
        print("")
        print("The minimum screen size required is:")
        print(f"  {self.min_cols} columns x {self.min_rows} rows")
        print("")
        print("Going back to menu.")
        print("")
        self.wait_for_enter()

    def print_screen_heading(self):
        """Clear the screen and print the screen heading at the top of the screen."""
        hdg = self.theme["default"] + screen.CLEAR + cur.HOME
        if self.show_heading:
            hdg += self.theme["screen_heading"]
            hdg += self.heading
            hdg += self.newline
            hdg += self.theme["screen_heading_divider"]
            hdg += "-" * self.screen_cols
            hdg += self.newline
        prnt(hdg)
        prnt(screen.title(self.heading))

    def wait_for_enter(self, prompt="Press ENTER to continue..."):
        """Prompt the user to press enter."""
        p = self.theme["press-enter"] + prompt + self.theme["default"] + "  "
        input(p)

    @staticmethod
    def finalize():
        """Reset the screen to user defaults."""
        codes = screen.RESET + c.DEFAULT_FG + c.DEFAULT_BG + screen.CLEAR + cur.HOME + cur.SHOW
        prnt(codes)

    @abstractmethod
    def run_demo(self):
        pass
