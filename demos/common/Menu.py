from easyansi.core import colors as c
from easyansi.core import screen
from easyansi.core import attributes as a
from easyansi.core import cursor as cur
from easyansi.core.utils import prnt

THEME = {"default": c.WHITE_FG + c.BLACK_BG,
         "title": c.BRIGHT_CYAN_FG + c.BLACK_BG,
         "title-underline": c.BRIGHT_GREEN_FG + c.BLACK_BG,
         "item-number": c.CYAN_FG + c.BLACK_BG,
         "item-separator": c.BRIGHT_GREEN_FG + c.BLACK_BG,
         "item-text": c.WHITE_FG + c.BLACK_BG,
         "prompt-text": c.CYAN_FG + c.BLACK_BG,
         "error": c.BRIGHT_YELLOW_FG + c.RED_BG
         }

NEWLINE = THEME["default"] + '\n'


class Menu:

    def __init__(self, title, items):
        self.title = title
        self.items = items
        self.clear_screen = True

    def display_menu(self):
        """Display the menu."""
        menu_text = THEME["default"]
        if self.clear_screen:
            menu_text += screen.CLEAR_SCREEN + cur.HOME_ON_SCREEN
            self.clear_screen = False
        menu_text += THEME["title"] + self.title + NEWLINE
        menu_text += THEME["title-underline"] + "=" * len(self.title) + NEWLINE * 2
        for idx, item in enumerate(self.items, start=1):
            menu_text += f"{THEME['item-number']}{idx:>3d}"
            menu_text += f"{THEME['item-separator']})"
            menu_text += f"{THEME['default']} "
            menu_text += f"{THEME['item-text']}{item}{NEWLINE}"
        menu_text += NEWLINE
        prnt(menu_text)
        prnt(screen.title(self.title))

    @staticmethod
    def display_choice():
        """Display the choice prompt."""
        prompt = "Enter Choice: "
        choice_text = f"{THEME['prompt-text']}{prompt}{THEME['default']}"
        user_choice = input(choice_text)
        return user_choice

    @staticmethod
    def display_invalid_choice():
        """Display an error for a bad choice."""
        error_text = NEWLINE
        error_text += THEME['error'] + "**********************" + NEWLINE
        error_text += THEME['error'] + "*** " + a.BLINK_ON + "INVALID CHOICE" + a.BLINK_OFF + " ***" + NEWLINE
        error_text += THEME['error'] + "**********************" + NEWLINE
        error_text += NEWLINE
        prnt(error_text)

    def get_choice(self):
        """Prompt the user for a choice until he/she selects a valid one."""
        choice = -1
        while choice < 0:
            self.display_menu()
            user_choice = Menu.display_choice()
            if user_choice.isnumeric():
                user_choice_int = int(user_choice)
                if 0 < user_choice_int <= len(self.items):
                    choice = user_choice_int - 1
            if choice < 0:
                Menu.display_invalid_choice()
        self.clear_screen = True
        return self.items[choice]
