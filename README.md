# Easy ANSI

![Easy ANSI Splash Screen](https://gitlab.com/easy-ansi/easy-ansi-demos/-/raw/main/images/easy_ansi_splash_screen.png)

Easy ANSI is a terminal framework API to give you an easy way to use colors, attributes, screen actions, and
cursor control movements.  This framework adheres to the ANSI standard codes, and does not implement
operating system specific features.  It uses only the standard Python library, and does not have external
dependencies such as ncurses.

Documentation can be found here: [Easy ANSI Documentation](https://gitlab.com/easy-ansi/easy-ansi/-/blob/main/docs/README.md)

The change log can be found here: [Easy ANSI Change Log](https://gitlab.com/easy-ansi/easy-ansi/-/blob/main/CHANGELOG.md)
