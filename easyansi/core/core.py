# Escape
ESC = '\033'

# Bell
BEL = '\007'

# String Terminator
ST = f"{ESC}\\"

# Control Sequence Introducer
CSI = f"{ESC}["

# Operating System Command
OSC = f"{ESC}]"

# Reset terminal to defaults
RESET = f"{CSI}0m"
