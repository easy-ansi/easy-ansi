#!/usr/bin/env bash
set -e

source ../common/common.sh
activate_conda_environment


function run_demos() {
  show_heading "Running Demos"
  cd "${DEMOS_DIR}"
  python menu.py
}


run_demos
