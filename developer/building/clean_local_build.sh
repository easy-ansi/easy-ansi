#!/usr/bin/env bash
set -e

source ../common/common.sh
activate_conda_environment


function clean_build_files() {
  show_heading "Clean Easy ANSI Locally Built Files"
  echo "Project dir: ${PROJECT_DIR}"
  echo ""

  delete_dir "build"
  delete_dir "dist"
  delete_dir "easy_ansi.egg-info"
}


function delete_dir() {
  local directory
  directory="${1}"
  if [ -d "${PROJECT_DIR}/${directory}" ]
  then
    echo "Deleting Dir: ${PROJECT_DIR}/${directory}"
    cd "${PROJECT_DIR}"
    rm -rf "${directory}"
  fi
}


clean_build_files
