# Release Checklist

- [ ] Update main README.md
- [ ] Update CHANGELOG.md
- [ ] Update setup.py 
  - [ ] Version number
  - [ ] Links
  - [ ] Classifiers
    - [ ] Development Status
    - [ ] Programming Language
- [ ] Close GitLab issues
- [ ] Merge to main
- [ ] Delete old build files
- [ ] Run build
- [ ] Test publish
- [ ] Production publish
- [ ] Tag Release
