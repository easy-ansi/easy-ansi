#!/usr/bin/env bash
set -e

source ../common/common.sh
activate_conda_environment

FLAKE8_DIR="${SCRIPTS_DIR}"
FLAKE8_INI="${FLAKE8_DIR}/flake8/flake8.ini"


function run_flake8() {
  show_heading "Running Flake8 Checks"
  echo "Flake8 ini:    ${FLAKE8_INI}"
  echo "Easy ANSI dir: ${EASYANSI_DIR}"
  echo ""
  set +e
  python3 -m flake8 --config="${FLAKE8_INI}" "${EASYANSI_DIR}"
  local flake8_status=$?
  set -e
  echo ""
  echo "Flake8 Exit Code: ${flake8_status}"
  echo ""
}


run_flake8
