#!/usr/bin/env bash
set -e

# Notes:
#   -s: add to command to see output of print statements
#   Required: https://pytest-cov.readthedocs.io/en/latest/ , pytest-cov

source ../common/common.sh
activate_conda_environment

function run_pytest() {
  show_heading "Running PyTest Tests"
  echo "Source dir:  ${EASYANSI_DIR}"
  echo "Tests dir:   ${TESTS_DIR}"
  echo "Project dir: ${PROJECT_DIR}"
  echo ""
  cd "${EASYANSI_DIR}"
  set +e
  python3 -m pytest --cache-clear --cov=easyansi --cov-report=term-missing "${TESTS_DIR}/"
  local pytest_status=$?
  set -e
  echo ""
  echo "PyTest Exit Code: ${pytest_status}"
  echo ""
}

function clean_up_cache() {
  show_heading "Clean Up PyTest Caches"
  cd "${PROJECT_DIR}"

  while IFS= read -r d
  do
    echo "Deleting PyTest Cache Directory: ${d}"
    rm -rf "${d}"
  done < <(find "${PROJECT_DIR}" -type d -name ".pytest_cache")

  while IFS= read -r f
  do
    echo "Deleting PyTest-Cov Database File: ${f}"
    rm "${f}"
  done < <(find "${PROJECT_DIR}" -type f -name ".coverage")

}

run_pytest
clean_up_cache
