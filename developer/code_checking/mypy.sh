#!/usr/bin/env bash
set -e

source ../common/common.sh
activate_conda_environment

MYPY_DIR="${SCRIPTS_DIR}"
MYPY_INI="${MYPY_DIR}/mypy/mypy.ini"


function run_mypy() {
  show_heading "Running MyPy Checks"
  echo "MyPy ini:      ${MYPY_INI}"
  echo "Easy ANSI dir: ${EASYANSI_DIR}"
  echo ""
  set +e
  python3 -m mypy --strict --config-file "${MYPY_INI}" "${EASYANSI_DIR}"
  local mypy_status=$?
  set -e
  echo ""
  echo "MyPy Exit Code: ${mypy_status}"
  echo ""
}


run_mypy
