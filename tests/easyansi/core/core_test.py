import pytest
from easyansi.core import core


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "constant, value",
    [(core.ESC, '\033'),
     (core.CSI, '\033' + "["),
     (core.ST, '\033' + "\\"),
     (core.BEL, '\007'),
     (core.OSC, '\033' + "]"),
     (core.RESET, '\033' + "[" + "0m")
     ])
def test_constants(constant, value):
    assert constant == value
