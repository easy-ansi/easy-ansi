import pytest
from easyansi.core import attributes as a


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "actual, csi_index",
    [(a.NORMAL, 22),
     (a.BRIGHT_OFF, 22),
     (a.DIM_OFF, 22),
     (a.ITALIC, 3),
     (a.ITALIC_ON, 3),
     (a.ITALIC_OFF, 23),
     (a.UNDERLINE, 4),
     (a.UNDERLINE_ON, 4),
     (a.UNDERLINE_OFF, 24),
     (a.BLINK, 5),
     (a.BLINK_ON, 5),
     (a.BLINK_OFF, 25),
     (a.REVERSE, 7),
     (a.REVERSE_ON, 7),
     (a.REVERSE_OFF, 27),
     (a.CONCEAL, 8),
     (a.CONCEAL_ON, 8),
     (a.CONCEAL_OFF, 28),
     (a.STRIKETHROUGH, 9),
     (a.STRIKETHROUGH_ON, 9),
     (a.STRIKETHROUGH_OFF, 29)
     ])
def test_constants(actual, csi_index):
    expected = '\033' + "[" + str(csi_index) + "m"
    assert actual == expected


@pytest.mark.parametrize(
    "actual, csi_index",
    [(a.BRIGHT, 1),
     (a.BRIGHT_ON, 1),
     (a.DIM, 2),
     (a.DIM_ON, 2)
     ])
def test_constants_brightness(actual, csi_index):
    expected = a.NORMAL + '\033' + "[" + str(csi_index) + "m"
    assert actual == expected


def test_constants_attributes_off():
    expected = ""
    csi_indexes = (22, 23, 24, 25, 27, 28, 29)
    for csi_index in csi_indexes:
        expected += '\033' + "[" + str(csi_index) + "m"
    assert a.ATTRIBUTES_OFF == expected

