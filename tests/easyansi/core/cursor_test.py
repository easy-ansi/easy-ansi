import pytest
from easyansi.core import core
from easyansi.core import cursor as c


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "constant, value",
    [(c.MIN_MOVEMENT, 1),
     (c.MIN_COL, 0),
     (c.MIN_ROW, 0),
     (c.UP, c.up(1)),
     (c.DOWN, c.down(1)),
     (c.LEFT, c.left(1)),
     (c.RIGHT, c.right(1)),
     (c.NEXT_LINE, c.next_line(1)),
     (c.PREVIOUS_LINE, c.previous_line(1)),
     (c.HOME, c.locate(0, 0)),
     (c.HOME_ON_SCREEN, c.locate(0, 0)),
     (c.HOME_ON_ROW, c.locate_col(0)),
     (c.HIDE, f"{core.CSI}?25l"),
     (c.HIDE_ON, f"{core.CSI}?25l"),
     (c.SHOW_OFF, f"{core.CSI}?25l"),
     (c.SHOW, f"{core.CSI}?25h"),
     (c.SHOW_ON, f"{core.CSI}?25h"),
     (c.HIDE_OFF, f"{core.CSI}?25h")
     ])
def test_constants(constant, value):
    assert constant == value


###########################################################################
# up
###########################################################################


def test_up_low_rows():
    expected = "Cursor up row count"
    with pytest.raises(ValueError) as ve:
        c.up(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_up_valid_rows():
    rows = 5
    expected = f"{core.CSI}{rows}A"
    actual = c.up(rows)
    assert actual == expected


def test_up_default_rows():
    expected = f"{core.CSI}1A"
    actual = c.up()
    assert actual == expected


###########################################################################
# down
###########################################################################


def test_down_low_rows():
    expected = "Cursor down row count"
    with pytest.raises(ValueError) as ve:
        c.down(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_down_valid_rows():
    rows = 7
    expected = f"{core.CSI}{rows}B"
    actual = c.down(rows)
    assert actual == expected


def test_down_default_rows():
    expected = f"{core.CSI}1B"
    actual = c.down()
    assert actual == expected


###########################################################################
# left
###########################################################################


def test_left_low_cols():
    expected = "Cursor left column count"
    with pytest.raises(ValueError) as ve:
        c.left(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_left_valid_cols():
    cols = 3
    expected = f"{core.CSI}{cols}D"
    actual = c.left(cols)
    assert actual == expected


def test_left_default_cols():
    expected = f"{core.CSI}1D"
    actual = c.left()
    assert actual == expected


###########################################################################
# right
###########################################################################


def test_right_low_cols():
    expected = "Cursor right column count"
    with pytest.raises(ValueError) as ve:
        c.right(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_right_valid_cols():
    cols = 3
    expected = f"{core.CSI}{cols}C"
    actual = c.right(cols)
    assert actual == expected


def test_right_default_cols():
    expected = f"{core.CSI}1C"
    actual = c.right()
    assert actual == expected


###########################################################################
# move
###########################################################################

@pytest.mark.parametrize(
    "cols, rows",
    [(None, None), (0, None), (None, 0), (0, 0),
     (False, False), (0, False), (False, 0),
     (False, None), (None, False)
    ])
def test_move_no_values(cols, rows):
    expected = "Cursor move values"
    with pytest.raises(ValueError) as ve:
        c.move(cols, rows)
    assert expected in str(ve.value)


def test_move_invalid_cols():
    expected = "Cursor"
    with pytest.raises(TypeError) as te:
        c.move("a", 1)
    assert expected in str(te.value)


def test_move_invalid_rows():
    expected = "Cursor"
    with pytest.raises(TypeError) as te:
        c.move(1, "a")
    assert expected in str(te.value)


@pytest.mark.parametrize(
    "cols, rows, expected",
    [(0, 5, c.down(5)),
     (0, -3, c.up(3)),
     (2, 0, c.right(2)),
     (-4, 0, c.left(4)),
     (1, 2, c.right(1) + c.down(2)),
     (3, -4, c.right(3) + c.up(4)),
     (-2, 3, c.left(2) + c.down(3)),
     (-1, -2, c.left(1) + c.up(2)),
    ])
def test_move_valid_values(cols, rows, expected):
    actual = c.move(cols, rows)
    assert actual == expected


###########################################################################
# next_line
###########################################################################


def test_next_line_low_rows():
    expected = "Cursor next line row count"
    with pytest.raises(ValueError) as ve:
        c.next_line(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_next_line_valid_rows():
    rows = 4
    expected = f"{core.CSI}{rows}E"
    actual = c.next_line(rows)
    assert actual == expected


def test_next_line_default_rows():
    expected = f"{core.CSI}1E"
    actual = c.next_line()
    assert actual == expected


###########################################################################
# previous_line
###########################################################################


def test_previous_line_low_rows():
    expected = "Cursor previous line row count"
    with pytest.raises(ValueError) as ve:
        c.previous_line(c.MIN_MOVEMENT - 1)
    assert expected in str(ve.value)


def test_previous_line_valid_rows():
    rows = 4
    expected = f"{core.CSI}{rows}F"
    actual = c.previous_line(rows)
    assert actual == expected


def test_previous_line_default_rows():
    expected = f"{core.CSI}1F"
    actual = c.previous_line()
    assert actual == expected


###########################################################################
# locate
###########################################################################


def test_locate_invalid_col():
    expected = "Cursor locate column value"
    with pytest.raises(ValueError) as ve:
        c.locate(c.MIN_COL - 1, c.MIN_ROW)
    assert expected in str(ve.value)


def test_locate_invalid_row():
    expected = "Cursor locate row value"
    with pytest.raises(ValueError) as ve:
        c.locate(c.MIN_COL, c.MIN_ROW - 1)
    assert expected in str(ve.value)


def test_locate_valid_values():
    col = 20
    row = 10
    expected = f"{core.CSI}11;21H"
    actual = c.locate(col, row)
    assert actual == expected


###########################################################################
# locate_col
###########################################################################


def test_locate_col_invalid_col():
    expected = "Cursor locate_col column value"
    with pytest.raises(ValueError) as ve:
        c.locate_col(c.MIN_COL - 1)
    assert expected in str(ve.value)


def test_locate_col_valid_values():
    col = 20
    expected = f"{core.CSI}21G"
    actual = c.locate_col(col)
    assert actual == expected


###########################################################################
# position
###########################################################################


def test_position(monkeypatch):
    # Mock the function that makes low-level terminal calls
    def mock_get_position_response():
        return "40;20"  # y;x on 1-based indexes

    monkeypatch.setattr(c, "_get_position_response", mock_get_position_response)
    # Run the test
    col, row = c.position()
    assert col == 19
    assert row == 39
