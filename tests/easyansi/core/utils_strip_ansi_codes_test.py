import pytest
from easyansi.core import core as c
from easyansi.core import screen as s
from easyansi.core import attributes as a
from easyansi.core import cursor as cur
from easyansi.core import utils
from easyansi.core.color_builder import ColorBuilder


###########################################################################
# ANSI code sequences to test
###########################################################################


p16 = ColorBuilder.PALETTE_16
p256 = ColorBuilder.PALETTE_256
prgb = ColorBuilder.PALETTE_RGB

all_ansi_csi_sequences = [
    c.RESET,

    s.CLEAR_SCREEN, s.CLEAR, s.CLEAR_SCREEN_FWD, s.CLEAR_FWD, s.CLEAR_SCREEN_BWD,
    s.CLEAR_BWD, s.CLEAR_ROW, s.CLEAR_ROW_FWD, s.CLEAR_ROW_BWD, s.BELL,

    a.NORMAL, a.BRIGHT, a.BRIGHT_ON, a.BRIGHT_OFF,
    a.DIM, a.DIM_ON, a.DIM_OFF,
    a.ITALIC, a.ITALIC_ON, a.ITALIC_OFF,
    a.UNDERLINE, a.UNDERLINE_ON, a.UNDERLINE_OFF,
    a.BLINK, a.BLINK_ON, a.BLINK_OFF,
    a.REVERSE, a.REVERSE_ON, a.REVERSE_OFF,
    a.CONCEAL, a.CONCEAL_ON, a.CONCEAL_OFF,
    a.STRIKETHROUGH, a.STRIKETHROUGH_ON, a.STRIKETHROUGH_OFF,

    cur.UP, cur.DOWN, cur.LEFT, cur.RIGHT, cur.NEXT_LINE, cur.PREVIOUS_LINE,
    cur.HOME, cur.HOME_ON_SCREEN, cur.HOME_ON_ROW, cur.HIDE, cur.HIDE_ON, cur.HIDE_OFF,
    cur.SHOW, cur.SHOW_ON, cur.SHOW_OFF,
    cur.up(5), cur.down(3), cur.left(11), cur.right(25), cur.next_line(5), cur.previous_line(3),
    cur.locate(20, 10), cur.locate_col(25),

    p16.color(p16.default_value), p16.color(None, p16.default_value),
    p16.color(p16.min_value), p16.color(None, p16.min_value),
    p16.color(p16.max_value), p16.color(None, p16.max_value),
    p16.color(p16.palette // 2), p16.color(None, p16.palette // 2),
    p256.color(p256.default_value), p256.color(None, p256.default_value),
    p256.color(p256.min_value), p256.color(None, p256.min_value),
    p256.color(p256.max_value), p256.color(None, p256.max_value),
    p256.color(p256.palette // 2), p256.color(None, p256.palette // 2),
    prgb.color(prgb.default_value), prgb.color(None, prgb.default_value),
    prgb.color(prgb.min_value), prgb.color(None, prgb.min_value),
    prgb.color(prgb.max_value), prgb.color(None, prgb.max_value),
    prgb.color(prgb.palette // 2), prgb.color(None, prgb.palette // 2),
]

all_ansi_osc_title_patterns = [
    {"window": True, "icon": True, "alt": True},
    {"window": True, "icon": True, "alt": False},
    {"window": False, "icon": True, "alt": True},
    {"window": False, "icon": True, "alt": False},
    {"window": True, "icon": False, "alt": True},
    {"window": True, "icon": False, "alt": False}
    ]


###########################################################################
# strip_ansi_codes: CSI
###########################################################################


@pytest.mark.parametrize(
    "non_text_value",
    [13,
     3.14,
     True,
     None
     ])
def test_strip_ansi_codes_invalid_values(non_text_value):
    expected = "Text to strip ANSI codes from"
    with pytest.raises(TypeError) as te:
        utils.strip_ansi_codes(non_text_value)
    assert expected in str(te.value)


@pytest.mark.parametrize(
    "ansi_code_sequence", all_ansi_csi_sequences)
def test_strip_ansi_codes_prefix(ansi_code_sequence):
    part1 = "maybe"
    expected = part1
    text = f"{ansi_code_sequence}{part1}"
    assert utils.strip_ansi_codes(text) == expected


@pytest.mark.parametrize(
    "ansi_code_sequence", all_ansi_csi_sequences)
def test_strip_ansi_codes_suffix(ansi_code_sequence):
    part1 = "maybe["
    expected = part1
    text = f"{part1}{ansi_code_sequence}"
    assert utils.strip_ansi_codes(text) == expected


@pytest.mark.parametrize(
    "ansi_code_sequence", all_ansi_csi_sequences)
def test_strip_ansi_codes_center(ansi_code_sequence):
    part1 = "maybe["
    part2 = "maybealso"
    expected = f"{part1}{part2}"
    text = f"{part1}{ansi_code_sequence}{part2}"
    assert utils.strip_ansi_codes(text) == expected


@pytest.mark.parametrize(
    "ansi_code_sequence", all_ansi_csi_sequences)
def test_strip_ansi_codes_mixed(ansi_code_sequence):
    part1 = "maybe["
    part2 = "maybealso"
    expected = f"{part1}{part2}"
    text = f"{ansi_code_sequence}{part1}{ansi_code_sequence}{part2}{ansi_code_sequence}"
    assert utils.strip_ansi_codes(text) == expected


###########################################################################
# strip_ansi_codes: OSC Title
###########################################################################


@pytest.mark.parametrize(
    "title_params", all_ansi_osc_title_patterns)
def test_strip_ansi_codes_osc_title_no_surrounding_text(title_params):
    title_text = "The Application Title"
    expected = title_text
    title_code = s.title(text=title_text, window=title_params["window"],
                         icon=title_params["icon"], alt=title_params["alt"])
    actual = utils.strip_ansi_codes(title_code)
    assert actual == expected


@pytest.mark.parametrize(
    "title_params", all_ansi_osc_title_patterns)
def test_strip_ansi_codes_osc_title_prefix_text(title_params):
    title_text = "The Application Title"
    prefix_text = "prefixText"
    expected = f"{prefix_text}{title_text}"
    title_code = s.title(text=title_text, window=title_params["window"],
                         icon=title_params["icon"], alt=title_params["alt"])
    full_text = f"{prefix_text}{title_code}"
    actual = utils.strip_ansi_codes(full_text)
    assert actual == expected


@pytest.mark.parametrize(
    "title_params", all_ansi_osc_title_patterns)
def test_strip_ansi_codes_osc_title_suffix_text(title_params):
    title_text = "The Application Title"
    suffix_text = "suffixText"
    expected = f"{title_text}{suffix_text}"
    title_code = s.title(text=title_text, window=title_params["window"],
                         icon=title_params["icon"], alt=title_params["alt"])
    full_text = f"{title_code}{suffix_text}"
    actual = utils.strip_ansi_codes(full_text)
    assert actual == expected


@pytest.mark.parametrize(
    "title_params", all_ansi_osc_title_patterns)
def test_strip_ansi_codes_osc_title_center_text(title_params):
    title_text = "The Application Title"
    prefix_text = "prefixText"
    suffix_text = "suffixText"
    expected = f"{prefix_text}{title_text}{suffix_text}"
    title_code = s.title(text=title_text, window=title_params["window"],
                         icon=title_params["icon"], alt=title_params["alt"])
    full_text = f"{prefix_text}{title_code}{suffix_text}"
    actual = utils.strip_ansi_codes(full_text)
    assert actual == expected


###########################################################################
# strip_ansi_codes: Mix it all up
###########################################################################


def test_strip_ansi_codes_some_of_everything():
    part1 = "Part1"
    part2 = "Part2"
    part3 = "Part3"
    part4 = "Part4"
    part5 = "Part5"
    part6 = "Part6"
    title = "The Title"
    title_code = s.title(title)
    full_text = (a.BRIGHT + part1 + p16.color(None, p16.min_value) + part2 + title_code +
                 part3 + cur.right(5) + part4 + s.CLEAR_FWD + part5 + s.BELL + part6 + s.BELL)
    expected = f"{part1}{part2}{title}{part3}{part4}{part5}{part6}"
