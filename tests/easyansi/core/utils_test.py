import pytest
from easyansi.core import utils
from easyansi.core import screen


###########################################################################
# prnt / prntln
###########################################################################


def test_prnt(capsys):
    text = "This is a test line."
    expected = text
    utils.prnt(text)
    actual = capsys.readouterr()
    assert actual.out == expected


def test_prntln(capsys):
    text = "This is a test line."
    expected = text + '\n'
    utils.prntln(text)
    actual = capsys.readouterr()
    assert actual.out == expected


###########################################################################
# text_len
###########################################################################


@pytest.mark.parametrize(
    "text",
    ["",
     "   ",
     "Hello",
     "Some longer string here."
     ])
def test_text_len_valid_values(text):
    expected = len(text)
    csi_text = screen.CLEAR_ROW_FWD + text + screen.CLEAR_ROW_BWD
    actual = utils.text_len(csi_text)
    assert actual == expected
