import pytest
from easyansi.core import screen


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "constant, value",
    [(screen.DEFAULT_COLS, 80),
     (screen.DEFAULT_ROWS, 24),
     (screen.CLEAR_SCREEN, '\033' + "[" + "2J"),
     (screen.CLEAR, '\033' + "[" + "2J"),
     (screen.CLEAR_SCREEN, screen.CLEAR),
     (screen.CLEAR_SCREEN_FWD, '\033' + "[" + "0J"),
     (screen.CLEAR_FWD, '\033' + "[" + "0J"),
     (screen.CLEAR_SCREEN_FWD, screen.CLEAR_FWD),
     (screen.CLEAR_SCREEN_BWD, '\033' + "[" + "1J"),
     (screen.CLEAR_BWD, '\033' + "[" + "1J"),
     (screen.CLEAR_SCREEN_BWD, screen.CLEAR_BWD),
     (screen.CLEAR_ROW, '\033' + "[" + "2K"),
     (screen.CLEAR_ROW_FWD, '\033' + "[" + "0K"),
     (screen.CLEAR_ROW_BWD, '\033' + "[" + "1K"),
     (screen.RESET, '\033' + "[" + "0m"),
     (screen.BELL, '\007')
     ])
def test_constants(constant, value):
    assert constant == value


###########################################################################
# get_size
###########################################################################


def test_get_size():
    cols, rows = screen.size()
    assert cols == screen.DEFAULT_COLS
    assert rows == screen.DEFAULT_ROWS


###########################################################################
# sufficient_size
###########################################################################


def test_sufficient_size_low_cols():
    expected = "Minimum number of columns"
    min_cols = 0
    min_rows = 1
    with pytest.raises(ValueError) as ve:
        screen.sufficient_size(min_cols, min_rows)
    assert expected in str(ve.value)


def test_sufficient_size_low_rows():
    expected = "Minimum number of rows"
    min_cols = 1
    min_rows = 0
    with pytest.raises(ValueError) as ve:
        screen.sufficient_size(min_cols, min_rows)
    assert expected in str(ve.value)


def test_sufficient_size_small_screen_cols():
    min_cols = 100
    min_rows = 20
    expected_cols = screen.DEFAULT_COLS
    expected_rows = screen.DEFAULT_ROWS
    expected_sufficient = False
    actual_sufficient, actual_cols, actual_rows = screen.sufficient_size(min_cols, min_rows)
    assert actual_sufficient == expected_sufficient
    assert actual_cols == expected_cols
    assert actual_rows == expected_rows


def test_sufficient_size_small_screen_rows():
    min_cols = 60
    min_rows = 30
    expected_cols = screen.DEFAULT_COLS
    expected_rows = screen.DEFAULT_ROWS
    expected_sufficient = False
    actual_sufficient, actual_cols, actual_rows = screen.sufficient_size(min_cols, min_rows)
    assert actual_sufficient == expected_sufficient
    assert actual_cols == expected_cols
    assert actual_rows == expected_rows


def test_sufficient_size_within_limits():
    min_cols = 60
    min_rows = 20
    expected_cols = screen.DEFAULT_COLS
    expected_rows = screen.DEFAULT_ROWS
    expected_sufficient = True
    actual_sufficient, actual_cols, actual_rows = screen.sufficient_size(min_cols, min_rows)
    assert actual_sufficient == expected_sufficient
    assert actual_cols == expected_cols
    assert actual_rows == expected_rows


def test_sufficient_size_exact_limits():
    min_cols = screen.DEFAULT_COLS
    min_rows = screen.DEFAULT_ROWS
    expected_cols = screen.DEFAULT_COLS
    expected_rows = screen.DEFAULT_ROWS
    expected_sufficient = True
    actual_sufficient, actual_cols, actual_rows = screen.sufficient_size(min_cols, min_rows)
    assert actual_sufficient == expected_sufficient
    assert actual_cols == expected_cols
    assert actual_rows == expected_rows


###########################################################################
# title
###########################################################################


def test_title_invalid_title():
    expected = "Screen title text"
    with pytest.raises(TypeError) as te:
        screen.title(5)
    assert expected in str(te.value)


def test_title_invalid_window_indicator():
    expected = "Screen title window indicator"
    with pytest.raises(TypeError) as te:
        screen.title("test", window=5)
    assert expected in str(te.value)


def test_title_invalid_icon_indicator():
    expected = "Screen title icon indicator"
    with pytest.raises(TypeError) as te:
        screen.title("test", icon=5)
    assert expected in str(te.value)


def test_title_invalid_alternate_code_indicator():
    expected = "Screen title alternate code indicator"
    with pytest.raises(TypeError) as te:
        screen.title("test", alt=5)
    assert expected in str(te.value)


def test_title_no_indicators_enabled():
    expected_1 = "Screen title indicators"
    expected_2 = "No title indicators set to be changed"
    with pytest.raises(ValueError) as ve:
        screen.title("test", window=False, icon=False)
    assert expected_1 in str(ve.value)
    assert expected_2 in str(ve.value)


@pytest.mark.parametrize(
    "text, window, icon, alt, operation",
    [("Title 0a", True, True, True, "0"),
     ("Title 0b", True, True, False, "0"),
     ("Title 1a", False, True, True, "1"),
     ("Title 1b", False, True, False, "1"),
     ("Title 2a", True, False, True, "2"),
     ("Title 2b", True, False, False, "2")
    ])
def test_title_codes(text, window, icon, alt, operation):
    expected = '\033' + "]" + operation + ";" + text
    if alt:
        expected += '\007'
    else:
        expected += '\033' + "\\"
    actual = screen.title(text=text, window=window, icon=icon, alt=alt)
    assert actual == expected


def test_title_default_parameters():
    title = "New Window Title"
    assert screen.title(title) == screen.title(text=title, window=True, icon=True, alt=False)
