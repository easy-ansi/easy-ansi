import pytest
from easyansi.common import regex_patterns as rp


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "constant, value",
    [(rp.ALL_CSI_REGEX, r'(\x1B\[)[0-?]*[ -\/]*[@-~]'),
     (rp.BELL_REGEX, r'\x07'),
     (rp.OSC_TITLE_REGEX, r'(.*)(\x1B\][012];)(.*)(\x1B\\|\x07)(.*)')
     ])
def test_constants(constant, value):
    assert constant == value


@pytest.mark.parametrize(
    "regex, pattern",
    [(rp.ALL_CSI_REGEX, rp.ALL_CSI_PATTERN),
     (rp.BELL_REGEX, rp.BELL_PATTERN),
     (rp.OSC_TITLE_REGEX, rp.OSC_TITLE_PATTERN)
     ])
def test_pattern_constants(regex, pattern):
    assert regex == pattern.pattern
