import pytest
from easyansi.common import field_validations as validator


###########################################################################
# _raise_error
###########################################################################


@pytest.mark.parametrize(
    "field, value,msg",
    [("Field1", "abc", "That value is wrong"),
     ("Field2", 3, "Bad Integer"),
     ("Field3", 3.14, "Bad Float"),
     ("Field4", None, "Bad None"),
     ("Field5", False, "Bad Boolean"),
     ])
def test_raise_error_msg_text(field, value, msg):
    expected = "Field: " + str(field) + ", Value: " + str(value) + ", Error: " + str(msg)
    with pytest.raises(RuntimeError) as re:
        validator._raise_error(field, value, msg, RuntimeError)
    assert str(re.value) == expected


###########################################################################
# check_if_boolean
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [("abc", "Field1"),
     (123, "Field2"),
     (None, "Field3"),
     (3.14, "Field4"),
     (0, "Field5"),
     (1, "Field6")
     ])
def test_check_if_boolean_invalid_values(value, field):
    expected = "Not a boolean value"
    with pytest.raises(TypeError) as te:
        validator.check_if_boolean(value, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "value, field",
    [(True, "Field1"),
     (False, "Field2"),
     ])
def test_check_if_boolean_valid_values(value, field):
    # test passes if no exception is raised
    validator.check_if_boolean(value, field)


###########################################################################
# check_if_string
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [(False, "Field1"),
     (123, "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_if_string_invalid_values(value, field):
    expected = "Not a string value"
    with pytest.raises(TypeError) as te:
        validator.check_if_string(value, field)
    assert expected in str(te.value)
    assert field in str(te.value)


def test_check_if_string_valid_value():
    # test passes if no exception is raised
    value = "abc"
    field = "Field1"
    validator.check_if_string(value, field)


###########################################################################
# check_if_int
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_if_int_invalid_values(value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_if_int(value, field)
    assert expected in str(te.value)
    assert field in str(te.value)


def test_check_if_int_valid_value():
    # test passes if no exception is raised
    value = 1
    field = "Field1"
    validator.check_if_int(value, field)


###########################################################################
# check_int_minimum_value
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_int_minimum_value_invalid_values(value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_int_minimum_value(value, 10, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "min_value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_int_minimum_value_invalid_minimum_values(min_value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_int_minimum_value(10, min_value, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "value, min_value, field",
    [(-1, 0, "field0"),
     (0, 1, "field1"),
     (10, 100, "field2"),
     (-10, -9, "field3")
    ])
def test_check_int_minimum_value_less_than_minimum_values(value, min_value, field):
    expected = f"Value is less than minimum of {min_value}"
    with pytest.raises(ValueError) as ve:
        validator.check_int_minimum_value(value, min_value, field)
    assert expected in str(ve.value)
    assert field in str(ve.value)


@pytest.mark.parametrize(
    "value, min_value, field",
    [(-1, -1, "field0"),
     (0, 0, "field1"),
     (1, 1, "field2"),
     (-8, -9, "field3"),
     (20, 19, "field4")
    ])
def test_check_int_minimum_value_valid_values(value, min_value, field):
    # If no exception is thrown, the test passes
    validator.check_int_minimum_value(value, min_value, field)


###########################################################################
# check_int_in_range
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_int_in_range_invalid_values(value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_int_in_range(value, 0, 10, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "min_value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_int_in_range_invalid_minimum_values(min_value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_int_in_range(5, min_value, 10, field)
    assert expected in str(te.value)
    assert f"Minimum for {field}" in str(te.value)


@pytest.mark.parametrize(
    "max_value, field",
    [(False, "Field1"),
     ("abc", "Field2"),
     (None, "Field3"),
     (3.14, "Field4")
     ])
def test_check_int_in_range_invalid_maximum_values(max_value, field):
    expected = "Not an int value"
    with pytest.raises(TypeError) as te:
        validator.check_int_in_range(5, 0, max_value, field)
    assert expected in str(te.value)
    assert f"Maximum for {field}" in str(te.value)


@pytest.mark.parametrize(
    "min_value, value, max_value, field",
    [(0, 20, 10, "Field1"),
     (10, 5, 20, "Field2"),
     (10, 20, 0, "Field3"),
     (20, 5, 10, "Field4")
     ])
def test_check_int_in_range_invalid_range_values(min_value, value, max_value, field):
    expected = f"Value is not within range of {min_value} to {max_value}"
    with pytest.raises(ValueError) as ve:
        validator.check_int_in_range(value, min_value, max_value, field)
    assert expected in str(ve.value)
    assert field in str(ve.value)


@pytest.mark.parametrize(
    "min_value, value, max_value, field",
    [(0, 5, 10, "Field1"),
     (0, 0, 10, "Field2"),
     (0, 10, 10, "Field3"),
     (5, 5, 5, "Field4")
     ])
def test_check_int_in_range_valid_range_values(min_value, value, max_value, field):
    # if no exception is raised, the test passes
    validator.check_int_in_range(value, min_value, max_value, field)


###########################################################################
# check_if_any_have_value
#   zero_int_no_value = True (zinv)
###########################################################################


def test_check_if_any_have_value_no_values():
    a = None
    b = None
    c = None
    field = "Field1"
    expected = "No provided arguments have a value"
    with pytest.raises(ValueError) as ve:
        validator.check_if_any_have_value(field, a, b, c)
    assert expected in str(ve.value)
    assert field in str(ve.value)


def test_check_if_any_have_value_no_values_list():
    a = None
    b = None
    c = None
    field = "Field1"
    expected = "No provided arguments have a value"
    with pytest.raises(ValueError) as ve:
        validator.check_if_any_have_value(field, *[a, b, c])
    assert expected in str(ve.value)
    assert field in str(ve.value)


has_value_list = [
    (1, None, None, "Field1a"),
    (1, 1, None, "Field1b"),
    (1, None, 1, "Field1c"),
    (1, 1, 1, "Field1d"),
    (None, 1, None, "Field1e"),
    (None, 1, 1, "Field1f"),
    (None, None, 1, "Field1g"),
    ("a", None, None, "Field2a"),
    ("a", "a", None, "Field2b"),
    ("a", None, "a", "Field2c"),
    ("a", "a", "a", "Field2d"),
    (None, "a", None, "Field2e"),
    (None, "a", "a", "Field2f"),
    (None, None, "a", "Field2g"),
    (True, None, None, "Field3a"),
    (True, True, None, "Field3b"),
    (True, None, True, "Field3c"),
    (True, True, True, "Field3d"),
    (None, True, None, "Field3d"),
    (None, True, None, "Field3e"),
    (None, True, True, "Field3f"),
    (None, None, True, "Field3g")
]


@pytest.mark.parametrize(
    "a, b, c, field", has_value_list)
def test_check_if_any_have_value_yes_values(a, b, c, field):
    # No exception raised means test passed
    validator.check_if_any_have_value(field, a, b, c)


@pytest.mark.parametrize(
    "a, b, c, field", has_value_list)
def test_check_if_any_have_value_yes_values_zinv(a, b, c, field):
    # No exception raised means test passed
    validator.check_if_any_have_value(field, a, b, c, zero_int_no_value=True)


@pytest.mark.parametrize(
    "a, b, c, field",
    [(0, 0, 0, "Field1a"),
     (None, None, None, "Field1b"),
     (0, None, None, "Field2a"),
     (None, 0, None, "Field2b"),
     (None, None, 0, "Field2c"),
     (0, 0, None, "Field3a"),
     (0, None, 0, "Field3b"),
     (None, 0, 0, "Field3c"),
     (False, False, False, "Field4a"),
     (None, None, None, "Field4b"),
     (False, None, None, "Field5a"),
     (None, False, None, "Field5b"),
     (None, None, False, "Field5c"),
     (False, False, None, "Field6a"),
     (False, None, False, "Field6b"),
     (None, False, False, "Field6c")
    ])
def test_check_if_any_have_value_no_values_with_zinv(a, b, c, field):
    expected = "No provided arguments have a value"
    with pytest.raises(ValueError) as ve:
        validator.check_if_any_have_value(field, a, b, c, zero_int_no_value=True)
    assert expected in str(ve.value)


###########################################################################
# check_if_string_matches_regex
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [(None, "field1"),
     (1, "field2"),
     (3.14, "field3"),
     (True, "field4")
    ])
def test_check_if_string_matches_regex_invalid_value_types(value, field):
    expected = "Not a string value"
    regex = "*"
    with pytest.raises(TypeError) as te:
        validator.check_if_string_matches_regex(value, regex, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "regex, field",
    [(None, "field1"),
     (1, "field2"),
     (3.14, "field3"),
     (True, "field4")
    ])
def test_check_if_string_matches_regex_invalid_regex_types(regex, field):
    expected = "Not a string value"
    value = "abc"
    with pytest.raises(TypeError) as te:
        validator.check_if_string_matches_regex(value, regex, field)
    assert expected in str(te.value)
    assert f"Regex for {field}" in str(te.value)


def test_check_if_string_matches_regex_invalid_value():
    value = "ABC"
    regex = "^[ABC]{4}$"
    field = "field1"
    expected = "Value is not in a valid format"
    with pytest.raises(ValueError) as ve:
        validator.check_if_string_matches_regex(value, regex, field)
    assert expected in str(ve.value)
    assert field in str(ve.value)


def test_check_if_string_matches_regex_valid_value():
    value = "ABC"
    regex = "^[ABC]{3}$"
    field = "field1"
    expected = "Value is not in a valid format"
    # No exception means the test passed
    validator.check_if_string_matches_regex(value, regex, field)


###########################################################################
# check_if_sequence
###########################################################################


@pytest.mark.parametrize(
    "value, field",
    [("abc", "field0"),
     (123, "field1"),
     (3.14, "field2"),
     (None, "field3"),
     (True, "field4"),
     ({1, 2, 3}, "field5"),
     ({"item1": "a", "item2": 2}, "field6")
    ])
def test_check_if_sequence_invalid_values(value, field):
    expected = "Value is not a supported sequence"
    with pytest.raises(TypeError) as te:
        validator.check_if_sequence(value, field)
    assert expected in str(te.value)
    assert field in str(te.value)


@pytest.mark.parametrize(
    "value, field",
    [((1, 2, 3), "field0"),
     (["a", "b", "c"], "field1"),
    ])
def test_check_if_sequence_valid_values(value, field):
    # No expections = test passes
    validator.check_if_sequence(value, field)
