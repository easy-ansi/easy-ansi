import pytest
from easyansi.core.core import CSI
from easyansi.core.color_builder import *

p256 = ColorBuilder.PALETTE_256


###########################################################################
# constants
###########################################################################


# These values must always be correct
@pytest.mark.parametrize(
    "actual, expected",
    [(p256.min_value, 0),
     (p256.max_value, 255),
     (p256.max_value_seq, 255),
     (p256.palette, 256),
     (p256.default_value, -1)
     ])
def test_256_color_constants(actual, expected):
    assert actual == expected


###########################################################################
# 256 color integer tests
###########################################################################


def test_int_256_foreground_color():
    color_value = 105
    assert p256.color(color_value, None) == f"{CSI}38;5;{color_value}m"


def test_int_256_background_color():
    color_value = 105
    assert p256.color(None, color_value) == f"{CSI}48;5;{color_value}m"


###########################################################################
# 256 color hex tests
###########################################################################


valid_hex_values = \
    ["12", "34", "56", "78", "90", "AB", "CD", "EF", "ab", "cd", "ef",
     " 12 ", " 34 ", " 56 ", " 78 ", " 90 ", " AB ", " CD ", " EF ", " ab ", " cd ", " ef ",
     "#12", "#34", "#56", "#78", "#90", "#AB", "#CD", "#EF", "#ab", "#cd", "#ef",
     " #12 ", " #34 ", " #56 ", " #78 ", " #90 ", " #AB ", " #CD ", " #EF ", " #ab ", " #cd ", " #ef ",
     "# 12", "# 34", "# 56", "# 78", "# 90", "# AB", "# CD", "# EF", "# ab", "# cd", "# ef",
     " # 12 ", " # 34 ", " # 56 ", " # 78 ", " # 90 ", " # AB ", " # CD ", " # EF ", " # ab ", " # cd ", " # ef ",
     "0x12", "0x34", "0x56", "0x78", "0x90", "0xAB", "0xCD", "0xEF", "0xab", "0xcd", "0xef",
     " 0x12 ", " 0x34 ", " 0x56 ", " 0x78 ", " 0x90 ", " 0xAB ", " 0xCD ", " 0xEF ", " 0xab ", " 0xcd ", " 0xef "]


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_256_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    p256.color(hex_value, None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_256_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    p256.color(None, hex_value)


invalid_hex_values = ["100", "ag", "-2", "-0xAB", "-#CD", "#0xBC", "0x#BC", "#", "0x", " "]


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_256_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        p256.color(hex_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_256_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        p256.color(None, hex_value)
    assert expected in str(ve.value)


def test_hex_256_foreground_color():
    color_value = "ab"
    int_value = int(color_value, 16)
    assert p256.color(color_value, None) == f"{CSI}38;5;{int_value}m"


def test_hex_256_background_color():
    color_value = "ab"
    int_value = int(color_value, 16)
    assert p256.color(None, color_value) == f"{CSI}48;5;{int_value}m"


###########################################################################
# color sequence tests - int
###########################################################################


def test_seq_int_256_foreground_color():
    color_value = 105
    assert p256.color((color_value,), None) == f"{CSI}38;5;{color_value}m"


def test_seq_int_256_background_color():
    color_value = 105
    assert p256.color(None, (color_value,)) == f"{CSI}48;5;{color_value}m"


###########################################################################
# color sequence tests - hex
###########################################################################


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_256_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    p256.color((hex_value,), None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_256_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    p256.color(None, (hex_value,))


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_256_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        p256.color((hex_value,), None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_256_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        p256.color(None, (hex_value,))
    assert expected in str(ve.value)


def test_seq_hex_256_foreground_color():
    color_value = ("ab",)
    int_value = int(color_value[0], 16)
    assert p256.color(color_value, None) == f"{CSI}38;5;{int_value}m"


def test_seq_hex_256_background_color():
    color_value = ("ab",)
    int_value = int(color_value[0], 16)
    assert p256.color(None, color_value) == f"{CSI}48;5;{int_value}m"
