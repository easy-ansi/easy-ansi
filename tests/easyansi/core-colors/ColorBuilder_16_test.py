import pytest
from easyansi.core.core import CSI
from easyansi.core.color_builder import *

p16 = ColorBuilder.PALETTE_16


###########################################################################
# constants
###########################################################################


# These values must always be correct
@pytest.mark.parametrize(
    "actual, expected",
    [(p16.min_value, 0),
     (p16.max_value, 15),
     (p16.max_value_seq, 15),
     (p16.palette, 16),
     (p16.default_value, -1)
     ])
def test_16_color_constants(actual, expected):
    assert actual == expected


###########################################################################
# 16 color integer tests
###########################################################################


valid_integer_foreground_values = [
    (0, 30), (1, 31), (2, 32), (3, 33), (4, 34), (5, 35), (6, 36), (7, 37),
    (8, 90), (9, 91), (10, 92), (11, 93), (12, 94), (13, 95), (14, 96), (15, 97)]


valid_integer_background_values = [
    (0, 40), (1, 41), (2, 42), (3, 43), (4, 44), (5, 45), (6, 46), (7, 47),
    (8, 100), (9, 101), (10, 102), (11, 103), (12, 104), (13, 105), (14, 106), (15, 107)]


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_integer_foreground_values)
def test_int_16_foreground_colors(color_value, ansi_value):
    assert p16.color(color_value, None) == f"{CSI}{ansi_value}m"


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_integer_background_values)
def test_int_16_background_colors(color_value, ansi_value):
    assert p16.color(None, color_value) == f"{CSI}{ansi_value}m"


###########################################################################
# 16 color hex tests
###########################################################################


valid_hex_values = \
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
     "A", "B", "C", "D", "E", "F",
     "a", "b", "c", "d", "e", "f",
     " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 ", " 0 ",
     " A ", " B ", " C ", " D ", " E ", " F ",
     " a ", " b ", " c ", " d ", " e ", " f ",
     "#1", "#2", "#3", "#4", "#5", "#6", "#7", "#8", "#9", "#0",
     "#A", "#B", "#C", "#D", "#E", "#F",
     "#a", "#b", "#c", "#d", "#e", "#f",
     " #1 ", " #2 ", " #3 ", " #4 ", " #5 ", " #6 ", " #7 ", " #8 ", " #9 ", " #0 ",
     " #A ", " #B ", " #C ", " #D ", " #E ", " #F ",
     " #a ", " #b ", " #c ", " #d ", " #e ", " #f ",
     "# 1", "# 2", "# 3", "# 4", "# 5", "# 6", "# 7", "# 8", "# 9", "# 0",
     "# A", "# B", "# C", "# D", "# E", "# F",
     "# a", "# b", "# c", "# d", "# e", "# f",
     " # 1 ", " # 2 ", " # 3 ", " # 4 ", " # 5 ", " # 6 ", " # 7 ", " # 8 ", " # 9 ", " # 0 ",
     " # A ", " # B ", " # C ", " # D ", " # E ", " # F ",
     " # a ", " # b ", " # c ", " # d ", " # e ", " # f ",
     "0x1", "0x2", "0x3", "0x4", "0x5", "0x6", "0x7", "0x8", "0x9", "0x0",
     "0xA", "0xB", "0xC", "0xD", "0xE", "0xF",
     "0xa", "0xb", "0xc", "0xd", "0xe", "0xf",
     " 0x1 ", " 0x2 ", " 0x3 ", " 0x4 ", " 0x5 ", " 0x6 ", " 0x7 ", " 0x8 ", " 0x9 ", " 0x0 ",
     " 0xA ", " 0xB ", " 0xC ", " 0xD ", " 0xE ", " 0xF ",
     " 0xa ", " 0xb ", " 0xc ", " 0xd ", " 0xe ", " 0xf "]


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_16_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    p16.color(hex_value, None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_16_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    p16.color(None, hex_value)


invalid_hex_values = ["10", "g", "-2", "-0xA", "-#C", "#0xB", "0x#B", "#", "0x", " "]


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_16_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        p16.color(hex_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_16_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        p16.color(None, hex_value)
    assert expected in str(ve.value)


valid_hex_foreground_values = [
    ("0", 30), ("1", 31), ("2", 32), ("3", 33), ("4", 34), ("5", 35), ("6", 36), ("7", 37),
    ("8", 90), ("9", 91), ("A", 92), ("B", 93), ("C", 94), ("D", 95), ("E", 96), ("F", 97)]


valid_hex_background_values = [
     ("0", 40), ("1", 41), ("2", 42), ("3", 43), ("4", 44), ("5", 45), ("6", 46), ("7", 47),
     ("8", 100), ("9", 101), ("a", 102), ("b", 103), ("c", 104), ("d", 105), ("e", 106), ("f", 107)]


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_hex_foreground_values)
def test_hex_16_foreground_colors(color_value, ansi_value):
    assert p16.color(color_value, None) == f"{CSI}{ansi_value}m"


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_hex_background_values)
def test_hex_16_background_colors(color_value, ansi_value):
    assert p16.color(None, color_value) == f"{CSI}{ansi_value}m"


###########################################################################
# color sequence tests - int
###########################################################################


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_integer_foreground_values)
def test_seq_int_16_foreground_colors(color_value, ansi_value):
    assert p16.color((color_value,), None) == f"{CSI}{ansi_value}m"


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_integer_background_values)
def test_seq_int_16_background_colors(color_value, ansi_value):
    assert p16.color(None, (color_value,)) == f"{CSI}{ansi_value}m"


###########################################################################
# color sequence tests - hex
###########################################################################


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_16_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    p16.color((hex_value,), None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_16_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    p16.color(None, (hex_value,))


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_16_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        p16.color((hex_value,), None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_16_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        p16.color(None, (hex_value,))
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_hex_foreground_values)
def test_seq_hex_16_foreground_colors(color_value, ansi_value):
    assert p16.color((color_value,), None) == f"{CSI}{ansi_value}m"


@pytest.mark.parametrize(
    "color_value, ansi_value", valid_hex_background_values)
def test_seq_hex_16_background_colors(color_value, ansi_value):
    assert p16.color(None, (color_value,)) == f"{CSI}{ansi_value}m"
