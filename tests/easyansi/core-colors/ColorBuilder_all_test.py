import pytest
from easyansi.core.core import CSI
from easyansi.core.color_builder import *
from easyansi.core import utils

p16 = ColorBuilder.PALETTE_16
p256 = ColorBuilder.PALETTE_256
prgb = ColorBuilder.PALETTE_RGB

all_palettes = (p16, p256, prgb)


###########################################################################
# utilities
###########################################################################


# Given a list of data items, return a list of (palette, data_item) pairs.
def merge_list_with_all_palettes(data_list):
    palette_list = []
    for palette in all_palettes:
        for data_item in data_list:
            palette_list.append((palette, data_item))
    return palette_list


###########################################################################
# constants
###########################################################################


# These values must always be correct
@pytest.mark.parametrize(
    "actual, expected",
    [(DEFAULT_IDX, -1),
     (DEFAULT_IDX_STR, "-1"),
     (DEFAULT_FG, f"{CSI}39m"),
     (DEFAULT_BG, f"{CSI}49m"),
     (DEFAULT, f"{CSI}39m"),
     (MIN_VALUE, 0),
     ])
def test_all_color_constants(actual, expected):
    assert actual == expected


###########################################################################
# "color" specific tests
###########################################################################


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_color_no_values(palette):
    expected = "Color Values"
    with pytest.raises(ValueError) as ve:
        palette.color(None, None)
    assert expected in str(ve.value)


invalid_color_types = merge_list_with_all_palettes(
    [3.14, False, {1, 2, 3}, {"item1": "value1"}])


@pytest.mark.parametrize(
    "palette, value", invalid_color_types)
def test_color_overall_invalid_foreground_values(palette, value):
    expected = "Foreground Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        palette.color(value, None)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


@pytest.mark.parametrize(
    "palette, value", invalid_color_types)
def test_color_overall_invalid_background_values(palette, value):
    expected = "Background Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        palette.color(None, value)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_color_fg_plus_bg(palette):
    fg = 5
    bg = 8
    fg_code = palette.color(fg, None)
    bg_code = palette.color(None, bg)
    fg_plus_bg_code = palette.color(fg, bg)
    assert fg_plus_bg_code == fg_code + bg_code


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_color_foreground_equality(palette):
    fg_value = 6
    fg_code_1 = palette.color(fg_value, None)
    fg_code_2 = palette.color(fg_value)
    fg_code_3 = palette.color(fg=fg_value)
    assert fg_code_1 == fg_code_2 == fg_code_3


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_color_background_equality(palette):
    bg_value = 6
    bg_code_1 = palette.color(None, bg_value)
    bg_code_3 = palette.color(bg=bg_value)
    assert bg_code_1 == bg_code_3


###########################################################################
# color integer tests
###########################################################################


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_low_range_foreground_colors(palette):
    color_value = DEFAULT_IDX - 1
    expected = "Color Foreground Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_low_range_background_colors(palette):
    color_value = DEFAULT_IDX - 1
    expected = "Color Background Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_high_range_foreground_colors(palette):
    color_value = palette.max_value_seq + 1
    expected = "Color Foreground Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_high_range_background_colors(palette):
    color_value = palette.max_value_seq + 1
    expected = "Color Background Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_default_foreground_colors(palette):
    assert palette.color(DEFAULT_IDX, None) == DEFAULT_FG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_default_background_colors(palette):
    assert palette.color(None, DEFAULT_IDX) == DEFAULT_BG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_int_default_all_colors(palette):
    assert palette.color(DEFAULT_IDX, DEFAULT_IDX) == DEFAULT_FG + DEFAULT_BG


###########################################################################
# color hex string tests
###########################################################################


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_hex_high_range_foreground_values(palette):
    color_value = hex(palette.max_value_seq + 1)
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_hex_high_range_background_values(palette):
    color_value = hex(palette.max_value_seq + 1)
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_hex_default_foreground_colors(palette):
    assert palette.color(DEFAULT_IDX_STR, None) == DEFAULT_FG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_hex_default_background_colors(palette):
    assert palette.color(None, DEFAULT_IDX_STR) == DEFAULT_BG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_hex_default_all_colors(palette):
    assert palette.color(DEFAULT_IDX_STR, DEFAULT_IDX_STR) == DEFAULT_FG + DEFAULT_BG


###########################################################################
# color sequence tests - int
###########################################################################


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_low_range_foreground_colors(palette):
    color_value = (DEFAULT_IDX - 1,)
    expected = "Color Foreground Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_low_range_background_colors(palette):
    color_value = (DEFAULT_IDX - 1,)
    expected = "Color Background Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_high_range_foreground_colors(palette):
    color_value = (palette.max_value_seq + 1,)
    expected = "Color Foreground Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_high_range_background_colors(palette):
    color_value = (palette.max_value_seq + 1,)
    expected = "Color Background Integer Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


invalid_sequence_value_types = merge_list_with_all_palettes(
    [(3.14,), (True,), (None,), ((1,),), [3.14], [False], [None], [[1]]])


@pytest.mark.parametrize(
    "palette, value", invalid_sequence_value_types)
def test_seq_all_invalid_foreground_sequence_value_types(palette, value):
    expected = "Foreground Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        palette.color(value, None)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


@pytest.mark.parametrize(
    "palette, value", invalid_sequence_value_types)
def test_seq_all_invalid_background_sequence_value_types(palette, value):
    expected = "Background Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        palette.color(None, value)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


invalid_sequence_lengths = merge_list_with_all_palettes(
    [(), [], (1, 2), [1, 2], (1, 2, 3), [1, 2, 3], (1, 2, 3, 4), [1, 2, 3, 4]])


@pytest.mark.parametrize(
    "palette, sequence", invalid_sequence_lengths)
def test_seq_all_foreground_invalid_sequence_lengths(palette, sequence):
    expected = "Color Foreground Sequence Length"
    if not (palette.palette == 16777216 and len(sequence) == 3):
        with pytest.raises(ValueError) as ve:
            palette.color(sequence, None)
        assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette, sequence", invalid_sequence_lengths)
def test_seq_all_background_invalid_sequence_lengths(palette, sequence):
    expected = "Color Background Sequence Length"
    if not (palette.palette == 16777216 and len(sequence) == 3):
        with pytest.raises(ValueError) as ve:
            palette.color(None, sequence)
        assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_default_foreground_colors(palette):
    assert palette.color((DEFAULT_IDX,), None) == DEFAULT_FG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_default_background_colors(palette):
    assert palette.color(None, (DEFAULT_IDX,)) == DEFAULT_BG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_int_default_all_colors(palette):
    assert palette.color((DEFAULT_IDX,), (DEFAULT_IDX,)) == DEFAULT_FG + DEFAULT_BG


###########################################################################
# color sequence tests - hex
###########################################################################


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_hex_high_range_foreground_values(palette):
    color_value = (hex(palette.max_value_seq + 1),)
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        palette.color(color_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_hex_high_range_background_values(palette):
    color_value = (hex(palette.max_value_seq + 1),)
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        palette.color(None, color_value)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_hex_default_foreground_colors(palette):
    assert palette.color((DEFAULT_IDX_STR,), None) == DEFAULT_FG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_hex_default_background_colors(palette):
    assert palette.color(None, (DEFAULT_IDX_STR,)) == DEFAULT_BG


@pytest.mark.parametrize(
    "palette", all_palettes)
def test_seq_hex_default_all_colors(palette):
    assert palette.color((DEFAULT_IDX_STR,), (DEFAULT_IDX_STR,)) == DEFAULT_FG + DEFAULT_BG
