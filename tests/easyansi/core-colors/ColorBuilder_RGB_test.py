import pytest
from easyansi.core.core import CSI
from easyansi.core.color_builder import *

prgb = ColorBuilder.PALETTE_RGB


###########################################################################
# constants
###########################################################################


# These values must always be correct
@pytest.mark.parametrize(
    "actual, expected",
    [(prgb.min_value, 0),
     (prgb.max_value, 255),
     (prgb.max_value_seq, 16777215),
     (prgb.palette, 16777216),
     (prgb.default_value, -1)
     ])
def test_rgb_color_constants(actual, expected):
    assert actual == expected


###########################################################################
# RGB color integer tests
###########################################################################


def test_int_rgb_foreground_color():
    color_value = 0x102030
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_int_rgb_background_color():
    color_value = 0xAABBCC
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"


###########################################################################
# RGB color hex tests
###########################################################################


valid_hex_values = \
    ["123456", "7890AB", "CDEFab", "cdef01",
     " 123456 ", " 7890AB ", " CDEFab ", " cdef01 ",
     "#123456", "#7890AB", "#CDEFab", "#cdef01",
     " #123456 ", "#7890AB ", "#CDEFab ", "#cdef01 ",
     " # 123456", " # 7890AB", " # CDEFab", " # cdef01",
     "  # 123456 ", " # 7890AB ", " # CDEFab ", " # cdef01 ",
     "0x123456", "0x7890AB", "0xCDEFab", "0xcdef01",
     " 0x123456 ", "0x7890AB ", "0xCDEFab ", "0xcdef01 "]


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_rgb_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    prgb.color(hex_value, None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_hex_rgb_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    prgb.color(None, hex_value)


invalid_hex_values = ["1000000", "abcdeg", "-2", "-0xABCDEF", "-#AAAAAA", "#0xBBBBBB", "0x#DDDDDD", "#", "0x", " "]


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_256_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        prgb.color(hex_value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_hex_256_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        prgb.color(None, hex_value)
    assert expected in str(ve.value)


def test_hex_rgb_foreground_color():
    color_value = "0x102030"
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_hex_rgb_background_color():
    color_value = "0xAABBCC"
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"


###########################################################################
# color sequence tests - int
###########################################################################


invalid_sequence_value_types = [(3.14, 1, 1), (1, 3.14, 1), (1, 1, 3.14),
                                (True, 1, 1), (1, True, 1), (1, 1, True),
                                (None, 1, 1), (1, None, 1), (1, 1, None),
                                ((1,), 1, 1), (1, (1,), 1), (1, 1, (1,)),
                                [3.14, 1, 1], [1, 3.14, 1], [1, 1, 3.14],
                                [True, 1, 1], [1, True, 1], [1, 1, True],
                                [None, 1, 1], [1, None, 1], [1, 1, None],
                                [[1,], 1, 1], [1, [1,], 1], [1, 1, [1,]]]


@pytest.mark.parametrize(
    "value", invalid_sequence_value_types)
def test_seq_rgb_invalid_foreground_sequence_value_types(value):
    expected = "Foreground Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        prgb.color(value, None)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


@pytest.mark.parametrize(
    "value", invalid_sequence_value_types)
def test_seq_rgb_invalid_background_sequence_value_types(value):
    expected = "Background Color Type"
    expected_2 = "Unknown color value type"
    with pytest.raises(TypeError) as te:
        prgb.color(None, value)
    assert expected in str(te.value)
    assert expected_2 in str(te.value)


invalid_sequence_value_ranges = [(-2, 1, 1), (1, -2, 1), (1, 1, -2),
                                 (256, 1, 1), (1, 256, 1), (1, 1, 256),
                                 [-2, 1, 1], [1, -2, 1], [1, 1, -2],
                                 [256, 1, 1], [1, 256, 1], [1, 1, 256]]


@pytest.mark.parametrize(
    "value", invalid_sequence_value_ranges)
def test_seq_rgb_invalid_foreground_sequence_value_ranges(value):
    expected = "Color Foreground Integer Value"
    with pytest.raises(ValueError) as ve:
        prgb.color(value, None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "value", invalid_sequence_value_ranges)
def test_seq_rgb_invalid_background_sequence_value_ranges(value):
    expected = "Color Background Integer Value"
    with pytest.raises(ValueError) as ve:
        prgb.color(None, value)
    assert expected in str(ve.value)


sequence_default_ranges = [(DEFAULT_IDX, 2, 2), (2, DEFAULT_IDX, 2), (2, 2, DEFAULT_IDX),
                           [DEFAULT_IDX, 2, 2], [2, DEFAULT_IDX, 2], [2, 2, DEFAULT_IDX]]


@pytest.mark.parametrize(
    "value", sequence_default_ranges)
def test_seq_rgb_valid_foreground_default_values(value):
    assert prgb.color(value, None) == DEFAULT_FG


@pytest.mark.parametrize(
    "value", sequence_default_ranges)
def test_seq_rgb_valid_background_default_values(value):
    assert prgb.color(None, value) == DEFAULT_BG


@pytest.mark.parametrize(
    "value", sequence_default_ranges)
def test_seq_rgb_valid_foreground_and_background_default_values(value):
    assert prgb.color(value, value) == DEFAULT_FG + DEFAULT_BG


def test_seq_int_rgb_foreground_color():
    color_value = (0x102030,)
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_seq_ints_rgb_foreground_color():
    color_value = (0x10, 0x20, 0x30)
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_seq_int_rgb_background_color():
    color_value = (0xAABBCC,)
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"


def test_seq_ints_rgb_background_color():
    color_value = (0xAA, 0xBB, 0xCC)
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"


###########################################################################
# color sequence tests - hex
###########################################################################


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_rgb_valid_foreground_hex_values(hex_value):
    # No exceptions mean test passes
    prgb.color((hex_value,), None)


@pytest.mark.parametrize(
    "hex_value", valid_hex_values)
def test_seq_hex_rgb_valid_background_hex_values(hex_value):
    # No exceptions mean test passes
    prgb.color(None, (hex_value,))


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_256_invalid_foreground_hex_values(hex_value):
    expected = "Color Foreground Hex String Value"
    with pytest.raises(ValueError) as ve:
        prgb.color((hex_value,), None)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "hex_value", invalid_hex_values)
def test_seq_hex_256_invalid_background_hex_values(hex_value):
    expected = "Color Background Hex String Value"
    with pytest.raises(ValueError) as ve:
        prgb.color(None, (hex_value,))
    assert expected in str(ve.value)


def test_seq_hex_rgb_foreground_color():
    color_value = ("0x102030",)
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_seq_hex_split_rgb_foreground_color():
    color_value = ("0x10", "0x20", "0x30")
    red = 0x10
    green = 0x20
    blue = 0x30
    assert prgb.color(color_value, None) == f"{CSI}38;2;{red};{green};{blue}m"


def test_seq_hex_rgb_background_color():
    color_value = ("0xAABBCC",)
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"


def test_seq_hex_split_rgb_background_color():
    color_value = ("0xAA", "0xBB", "0xCC")
    red = 0xAA
    green = 0xBB
    blue = 0xCC
    assert prgb.color(None, color_value) == f"{CSI}48;2;{red};{green};{blue}m"
