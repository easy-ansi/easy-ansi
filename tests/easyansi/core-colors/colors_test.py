import pytest
from easyansi.core import colors as c
from easyansi.core.color_builder import ColorBuilder
cb16 = ColorBuilder.PALETTE_16


###########################################################################
# constants
###########################################################################

@pytest.mark.parametrize(
    "actual, expected",
    [(c.DEFAULT_IDX, -1),
     (c.BLACK_IDX, 0),
     (c.RED_IDX, 1),
     (c.GREEN_IDX, 2),
     (c.YELLOW_IDX, 3),
     (c.BLUE_IDX, 4),
     (c.MAGENTA_IDX, 5),
     (c.CYAN_IDX, 6),
     (c.WHITE_IDX, 7),
     (c.BRIGHT_BLACK_IDX, 8),
     (c.BRIGHT_RED_IDX, 9),
     (c.BRIGHT_GREEN_IDX, 10),
     (c.BRIGHT_YELLOW_IDX, 11),
     (c.BRIGHT_BLUE_IDX, 12),
     (c.BRIGHT_MAGENTA_IDX, 13),
     (c.BRIGHT_CYAN_IDX, 14),
     (c.BRIGHT_WHITE_IDX, 15)
    ])
def test_index_constants(actual, expected):
    assert actual == expected


@pytest.mark.parametrize(
    "actual, index",
    [(c.DEFAULT_FG, c.DEFAULT_IDX),
     (c.BLACK_FG, c.BLACK_IDX),
     (c.RED_FG, c.RED_IDX),
     (c.GREEN_FG, c.GREEN_IDX),
     (c.YELLOW_FG, c.YELLOW_IDX),
     (c.BLUE_FG, c.BLUE_IDX),
     (c.MAGENTA_FG, c.MAGENTA_IDX),
     (c.CYAN_FG, c.CYAN_IDX),
     (c.WHITE_FG, c.WHITE_IDX),
     (c.BRIGHT_BLACK_FG, c.BRIGHT_BLACK_IDX),
     (c.BRIGHT_RED_FG, c.BRIGHT_RED_IDX),
     (c.BRIGHT_GREEN_FG, c.BRIGHT_GREEN_IDX),
     (c.BRIGHT_YELLOW_FG, c.BRIGHT_YELLOW_IDX),
     (c.BRIGHT_BLUE_FG, c.BRIGHT_BLUE_IDX),
     (c.BRIGHT_MAGENTA_FG, c.BRIGHT_MAGENTA_IDX),
     (c.BRIGHT_CYAN_FG, c.BRIGHT_CYAN_IDX),
     (c.BRIGHT_WHITE_FG, c.BRIGHT_WHITE_IDX)
    ])
def test_fg_constants(actual, index):
    expected = cb16.color(index)
    assert actual == expected


@pytest.mark.parametrize(
    "actual, index",
    [(c.DEFAULT_BG, c.DEFAULT_IDX),
     (c.BLACK_BG, c.BLACK_IDX),
     (c.RED_BG, c.RED_IDX),
     (c.GREEN_BG, c.GREEN_IDX),
     (c.YELLOW_BG, c.YELLOW_IDX),
     (c.BLUE_BG, c.BLUE_IDX),
     (c.MAGENTA_BG, c.MAGENTA_IDX),
     (c.CYAN_BG, c.CYAN_IDX),
     (c.WHITE_BG, c.WHITE_IDX),
     (c.BRIGHT_BLACK_BG, c.BRIGHT_BLACK_IDX),
     (c.BRIGHT_RED_BG, c.BRIGHT_RED_IDX),
     (c.BRIGHT_GREEN_BG, c.BRIGHT_GREEN_IDX),
     (c.BRIGHT_YELLOW_BG, c.BRIGHT_YELLOW_IDX),
     (c.BRIGHT_BLUE_BG, c.BRIGHT_BLUE_IDX),
     (c.BRIGHT_MAGENTA_BG, c.BRIGHT_MAGENTA_IDX),
     (c.BRIGHT_CYAN_BG, c.BRIGHT_CYAN_IDX),
     (c.BRIGHT_WHITE_BG, c.BRIGHT_WHITE_IDX)
    ])
def test_bg_constants(actual, index):
    expected = cb16.color(None, index)
    assert actual == expected


@pytest.mark.parametrize(
    "expected, actual",
    [(c.DEFAULT_FG, c.DEFAULT),
     (c.BLACK_FG, c.BLACK),
     (c.RED_FG, c.RED),
     (c.GREEN_FG, c.GREEN),
     (c.YELLOW_FG, c.YELLOW),
     (c.BLUE_FG, c.BLUE),
     (c.MAGENTA_FG, c.MAGENTA),
     (c.CYAN_FG, c.CYAN),
     (c.WHITE_FG, c.WHITE),
     (c.BRIGHT_BLACK_FG, c.BRIGHT_BLACK),
     (c.BRIGHT_RED_FG, c.BRIGHT_RED),
     (c.BRIGHT_GREEN_FG, c.BRIGHT_GREEN),
     (c.BRIGHT_YELLOW_FG, c.BRIGHT_YELLOW),
     (c.BRIGHT_BLUE_FG, c.BRIGHT_BLUE),
     (c.BRIGHT_MAGENTA_FG, c.BRIGHT_MAGENTA),
     (c.BRIGHT_CYAN_FG, c.BRIGHT_CYAN),
     (c.BRIGHT_WHITE_FG, c.BRIGHT_WHITE)
    ])
def test_fg2_constants(expected, actual):
    assert actual == expected
