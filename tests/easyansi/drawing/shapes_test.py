import pytest

from easyansi.core import cursor as cur
from easyansi.drawing.shapes import Shapes
from easyansi.drawing.lines import Lines
from easyansi.drawing.line_chars import LineChars
from easyansi.drawing.shapes import MIN_WIDTH, MIN_HEIGHT


# These must be in the same order
SHAPE_ENUMS = (Shapes.ASCII, Shapes.BLOCK, Shapes.SINGLE, Shapes.DOUBLE)
MATCHED_ENUMS = ((Shapes.ASCII, Lines.ASCII, LineChars.ASCII),
                 (Shapes.BLOCK, Lines.BLOCK, LineChars.BLOCK),
                 (Shapes.SINGLE, Lines.SINGLE, LineChars.SINGLE),
                 (Shapes.DOUBLE, Lines.DOUBLE, LineChars.DOUBLE))

###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "actual, expected",
    [(MIN_WIDTH, 2),
     (MIN_HEIGHT, 2)
    ])
def test_constants(actual, expected):
    assert actual == expected


###########################################################################
# rectangle
###########################################################################


@pytest.mark.parametrize(
    "shape_enum", SHAPE_ENUMS)
def test_rectangle_width_too_low(shape_enum):
    expected = "Rectangle Width"
    with pytest.raises(ValueError) as ve:
        shape_enum.rectangle(MIN_WIDTH - 1, MIN_HEIGHT)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "shape_enum", SHAPE_ENUMS)
def test_rectangle_height_too_low(shape_enum):
    expected = "Rectangle Height"
    with pytest.raises(ValueError) as ve:
        shape_enum.rectangle(MIN_WIDTH, MIN_HEIGHT - 1)
    assert expected in str(ve.value)


@pytest.mark.parametrize(
    "shape_enum, line_enum, line_char_enum", MATCHED_ENUMS)
def test_rectangle_no_sides(shape_enum, line_enum, line_char_enum):
    expected = line_char_enum.top_left + line_char_enum.top_right
    expected += cur.move(-2, 1) + line_char_enum.bottom_left + line_char_enum.bottom_right
    actual = shape_enum.rectangle(MIN_WIDTH, MIN_HEIGHT)
    assert actual == expected


@pytest.mark.parametrize("shape_enum, line_enum, line_char_enum", MATCHED_ENUMS)
@pytest.mark.parametrize("width, height", [(3, 5), (5, 3), (4, 6), (6, 4)])
def test_rectangle_with_sides(shape_enum, line_enum, line_char_enum, width, height):
    # top line
    expected = line_char_enum.top_left + line_enum.horizontal(width - 2) + line_char_enum.top_right
    # left side
    expected += cur.move((-1 * width), 1) + line_enum.vertical(height - 2)
    # right side
    expected += cur.move((width - 2), (-1 *(height - 2 - 1))) + line_enum.vertical((height - 2))
    # bottom line
    expected += cur.move((-1 * width), 1) + line_char_enum.bottom_left
    expected += line_enum.horizontal(width - 2) + line_char_enum.bottom_right
    actual = shape_enum.rectangle(width, height)
    assert actual == expected


###########################################################################
# square
###########################################################################


@pytest.mark.parametrize(
    "shape_enum", SHAPE_ENUMS)
def test_square_size_too_low(shape_enum):
    expected = "Rectangle Width"
    with pytest.raises(ValueError) as ve:
        shape_enum.square(MIN_WIDTH - 1)
    assert expected in str(ve.value)


@pytest.mark.parametrize("shape_enum", SHAPE_ENUMS)
@pytest.mark.parametrize("size", [2, 3, 4, 5])
def test_square_equals_rectangle(shape_enum, size):
    assert shape_enum.square(size) == shape_enum.rectangle(size, size)
