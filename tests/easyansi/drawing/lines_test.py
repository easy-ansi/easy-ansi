import pytest
from easyansi.drawing.lines import Lines
from easyansi.drawing.lines import MIN_LENGTH
from easyansi.core import cursor as cur

LINE_ENUMS = (Lines.ASCII, Lines.BLOCK, Lines.SINGLE, Lines.DOUBLE)


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize(
    "actual, expected",
    [(MIN_LENGTH, 1)
    ])
def test_constants(actual, expected):
    assert actual == expected


###########################################################################
# horizontal
###########################################################################


@pytest.mark.parametrize(
    "line_enum", LINE_ENUMS)
def test_horizontal_length_too_low(line_enum):
    expected = "Horizontal Line Length"
    with pytest.raises(ValueError) as ve:
        line_enum.horizontal(MIN_LENGTH - 1)
    assert expected in str(ve.value)


@pytest.mark.parametrize("line_enum", LINE_ENUMS)
@pytest.mark.parametrize("line_length", (1, 5, 9))
def test_horizontal_single_char_default_line(line_enum, line_length):
    expected = line_enum._char_set.horizontal * line_length
    actual = line_enum.horizontal(line_length)
    assert actual == expected


###########################################################################
# vertical
###########################################################################


@pytest.mark.parametrize(
    "line_enum", LINE_ENUMS)
def test_vertical_length_too_low(line_enum):
    expected = "Vertical Line Length"
    with pytest.raises(ValueError) as ve:
        line_enum.vertical(MIN_LENGTH - 1)
    assert expected in str(ve.value)


@pytest.mark.parametrize("line_enum", LINE_ENUMS)
@pytest.mark.parametrize("line_length", (1, 5, 9))
def test_vertical_single_char_default_line(line_enum, line_length):
    cursor_down = cur.move(-1, 1)
    expected = ""
    if line_length > 1:
        for i in range(0, line_length - 1):
            expected += line_enum._char_set.vertical
            expected += cursor_down
    expected += line_enum._char_set.vertical
    actual = line_enum.vertical(line_length)
    assert actual == expected
