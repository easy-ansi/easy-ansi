import pytest
from easyansi.drawing.line_chars import LineChars
from easyansi.drawing.line_chars import LINE_CHARS_KEYS

CHARS_ENUMS = (LineChars.ASCII, LineChars.BLOCK, LineChars.SINGLE, LineChars.DOUBLE)

CHARS_KEYS = LINE_CHARS_KEYS

# These must be in the same order as CHARS_KEYS
ASCII_VALUES = ("+", "+", "+", "+",
                "+", "+", "+", "+", "+",
                "-", "|",
                "/", "\\", "X")

BLOCK_VALUES = ("\u2588", "\u2588", "\u2588", "\u2588",
                "\u2588", "\u2588", "\u2588", "\u2588", "\u2588",
                "\u2588", "\u2588",
                "\u2588", "\u2588", "\u2588")

SINGLE_VALUES = ("\u250C", "\u2510", "\u2514", "\u2518",
                 "\u253C", "\u251C", "\u2524", "\u252C", "\u2534",
                 "\u2500", "\u2502",
                 "\u2571", "\u2572", "\u2573")

SINGLE_MIXED_VALUES = ("\u250C", "\u2510", "\u2514", "\u2518",
                       "\u256C", "\u255E", "\u2561", "\u2565", "\u2568",
                       "\u2500", "\u2502",
                       "\u2571", "\u2572", "\u2573")

DOUBLE_VALUES = ("\u2554", "\u2557", "\u255A", "\u255D",
                 "\u256C", "\u2560", "\u2563", "\u2566", "\u2569",
                 "\u2550", "\u2551",
                 "\u2571", "\u2572", "\u2573")

DOUBLE_MIXED_VALUES = ("\u2554", "\u2557", "\u255A", "\u255D",
                       "\u253C", "\u255F", "\u2562", "\u2564", "\u2567",
                       "\u2550", "\u2551",
                       "\u2571", "\u2572", "\u2573")

# This has to be in the same order as CHARS_ENUMS
CHARS_ENUMS_VALUES = (ASCII_VALUES, BLOCK_VALUES, SINGLE_VALUES, DOUBLE_VALUES)

###########################################################################
# all_chars
###########################################################################


@pytest.mark.parametrize(
    "chars_enum", CHARS_ENUMS)
def test_all_chars_dictionary_length(chars_enum):
    assert len(chars_enum.all_chars) == len(CHARS_KEYS)


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
@pytest.mark.parametrize("chars_keys", CHARS_KEYS)
def test_all_chars_keys(chars_enum, chars_keys):
    assert chars_keys in chars_enum.all_chars.keys()


###########################################################################
# properties
###########################################################################


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_top_left_property(chars_enum):
    assert chars_enum.top_left == chars_enum.all_chars["top_left"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_top_right_property(chars_enum):
    assert chars_enum.top_right == chars_enum.all_chars["top_right"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_bottom_left_property(chars_enum):
    assert chars_enum.bottom_left == chars_enum.all_chars["bottom_left"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_bottom_right_property(chars_enum):
    assert chars_enum.bottom_right == chars_enum.all_chars["bottom_right"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_center_cross_property(chars_enum):
    assert chars_enum.center_cross == chars_enum.all_chars["center_cross"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_left_cross_property(chars_enum):
    assert chars_enum.left_cross == chars_enum.all_chars["left_cross"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_right_cross_property(chars_enum):
    assert chars_enum.right_cross == chars_enum.all_chars["right_cross"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_top_cross_property(chars_enum):
    assert chars_enum.top_cross == chars_enum.all_chars["top_cross"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_bottom_cross_property(chars_enum):
    assert chars_enum.bottom_cross == chars_enum.all_chars["bottom_cross"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_horizontal_property(chars_enum):
    assert chars_enum.horizontal == chars_enum.all_chars["horizontal"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_vertical_property(chars_enum):
    assert chars_enum.vertical == chars_enum.all_chars["vertical"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_diagonal_fwd_property(chars_enum):
    assert chars_enum.diagonal_fwd == chars_enum.all_chars["diagonal_fwd"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_diagonal_bwd_property(chars_enum):
    assert chars_enum.diagonal_bwd == chars_enum.all_chars["diagonal_bwd"]


@pytest.mark.parametrize("chars_enum", CHARS_ENUMS)
def test_diagonal_cross_property(chars_enum):
    assert chars_enum.diagonal_cross == chars_enum.all_chars["diagonal_cross"]


###########################################################################
# constants
###########################################################################


@pytest.mark.parametrize("enum_idx", list(range(0, len(CHARS_ENUMS))))
@pytest.mark.parametrize("key_idx", list(range(0, len(CHARS_KEYS))))
def test_chars_enum_values(enum_idx, key_idx):
    char_enum = CHARS_ENUMS[enum_idx]
    char_dict = CHARS_ENUMS_VALUES[enum_idx]
    key_name = CHARS_KEYS[key_idx]
    expected = char_dict[key_idx]
    actual = char_enum.all_chars[key_name]
    assert actual == expected
