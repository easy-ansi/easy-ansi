# Easy ANSI: Shapes Enum

## Description

The Shapes enum gives you functions for drawing shapes on the screen.

## Usage

```python
from easyansi.drawing.shapes import Shapes

ascii = Shapes.ASCII
block = Shapes.BLOCK
single = Shapes.SINGLE
double = Shapes.DOUBLE
```

## Functions

| Function | Parameters | Default Values | Description |
| --- | --- | --- | --- |
| rectangle() | width<br />height | n/a<br />n/a | Draws a rectangle of a given width and height. |
| square() | size | n/a | Draws a square of a given size.<br />This is really a convenience method for calling rectangle with the same width and height. |

## Constants

| Constant | Description |
| --- | --- |
| MIN_WIDTH | The minimum width a rectangular shape can be. |
| MIN_HEIGHT | The minimum height a rectangular shape can be. |
