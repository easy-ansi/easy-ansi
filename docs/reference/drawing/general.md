# Easy ANSI: General Drawing Information

## Drawing Character Templates

| Type | Character Set | Description |
| --- | --- | --- |
| ASCII | Keyboard | Drawing done only with characters that can be typed on the keyboard. |
| BLOCK | Unicode | Drawing done solely with 1 full-sized block character. |
| SINGLE | Unicode | Drawing done with single-line characters. |
| DOUBLE | Unicode | Drawing done with double-line characters. |
