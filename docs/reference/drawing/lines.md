# Easy ANSI: Lines Enum

## Description

The Lines enum gives you functions for drawing horizontal and vertical lines.

## Usage

```python
from easyansi.drawing.lines import Lines

ascii = Lines.ASCII
block = Lines.BLOCK
single = Lines.SINGLE
double = Lines.DOUBLE
```

## Functions

| Function | Parameters | Default Values | Description |
| --- | --- | --- | --- |
| horizontal() | length | n/a | Draw a horizontal line of a given length. |
| vertical() | length | n/a | Draw a vertical line of a given length.  This does utilize cursor control movements. |

## Constants

| Constant | Description |
| --- | --- |
| MIN_LENGTH | The minimum length a line can be. |
