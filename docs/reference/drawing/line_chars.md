# Easy ANSI: LineChars Enum

## Description

The Line Characters enum contains individual characters for drawing lines and line-based shapes.

## Usage

```python
from easyansi.drawing.line_chars import LineChars

ascii_chars = LineChars.ASCII
block_chars = LineChars.BLOCK
single_chars = LineChars.SINGLE
double_chars = LineChars.DOUBLE
```

## Line Character Dictionary

| Property | Key | Description |
| --- | --- | --- |
| all_chars | n/a | Returns the dictionary of the full character set. |
| bottom_cross | bottom_cross | Intersection of a bottom-side horizontal character and a middle-vertical character. |
| bottom_left | bottom_left | Bottom-left corner of a rectangle. |
| bottom_right | bottom_right | Bottom-right corner of a rectangle. |
| center_cross | center_cross | Perpendicular intersection of a horizontal and vertical line character. |
| diagonal_bwd | diagonal_bwd | A "backward" slash line character (top-left to bottom-right direction). |
| diagonal_cross | diagonal_cross | Intersection of a forward diagonal and backward diagonal line character. |
| diagonal_fwd | diagonal_fwd | A "forward" slash line character (bottom-left to top-right direction). |
| horizontal | horizontal | A horizontal line character. |
| left_cross | left_cross | Intersection of a left-side character and a middle-horizontal line character. |
| right_cross | right_cross | Intersection of a middle-horizontal line character and a right-side character. |
| top_cross | top_cross | Intersection of a top-side horizontal character and a middle-vertical character. |
| top_left | top_left | Top-left corner of a rectangle. |
| top_right | top_right | Top-right corner of a rectangle. |
| vertical | vertical | A vertical line character. |

## Constants

| Constant | Description |
| --- | --- |
| LINE_CHARS_KEYS | A tuple of all the keys available in the drawing set. |