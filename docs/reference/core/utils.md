# Easy ANSI: utils module

## Description

The _utils_ module is a collection of functions to assist with your programs which use Easy ANSI.

## Usage

```python
from easyansi.core import utils
```

## Constants

No constants.

## Functions

| Function | Parameters | Valid Values | Description |
| --- | --- | ---| --- |
| prnt | text | Any text string | This is a wrapper around print which will _NOT_ output a newline, and it will force a flush of the buffer.  This can be useful for making sure your output shows on the terminal as quickly as possible, without waiting for an automatic flush. |
| prntln | text | Any text string | This is identical to prnt, except it _WILL_ output a newline. |
| strip_ansi_codes | text | Any text string | Given a text string, this function will search for ANSI code sequences and remove them from the text.  It is only guaranteed to remove codes that Easy ANSI can generate, though in general it will detect more within the same ANSI sequence families. |
| text_len | text | Any text string | Given a text string, return the length of the text without any ANSI codes which may be embedded in it.  This is a convenience method to calling strip_ansi_codes and returning the length of the resulting text. |
