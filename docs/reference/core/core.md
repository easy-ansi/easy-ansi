# Easy ANSI: core module

## Description

Most of Easy ANSI utilizes the _core_ module, but it is unlikely you would ever need to use this module in your own
programs. 

## Usage

```python
from easyansi.core import core
```

## Constants

| Constant | Description |
| --- | --- |
| EMPTY | An empty string. |
| SPACE | A single space. |
| ESC | The escape character code. |
| CSI | The ANSI CSI prefix. |
| RESET | ANSI string to reset your terminal to its defaults. |

## Functions

No functions.
