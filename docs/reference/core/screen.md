# Easy ANSI: screen module

## Description

The _screen_ module is a collection of constants and functions related to screen clearing and screen properties
in general. 

## Usage

```python
from easyansi.core import screen
```

## Constants

| Constant | Description |
| --- | --- |
| BELL | Play the terminal bell (system beep). |
| CLEAR<br>CLEAR_SCREEN | Clear the entire screen. |
| CLEAR_FWD<br>CLEAR_SCREEN_FWD | Clear the screen from the current cursor position forward. |
| CLEAR_BWD<br>CLEAR_SCREEN_BWD | Clear the screen from the current cursor position backward. |
| CLEAR_ROW | Clear the entire row at the current cursor position. |
| CLEAR_ROW_FWD | Clear the row from the current cursor position forward. |
| CLEAR_ROW_BWD | Clear the row from the current cursor position backward. |
| DEFAULT_COLS | Many terminals will default to 80 columns of text. |
| DEFAULT_ROWS | Many terminals will default to 24 rows of text. |
| RESET | See core.RESET for description. |

## Functions

| Function | Parameters | Valid Values | Description |
| --- | --- | ---| --- |
| size() | _n/a_ | _n/a_ | This is not an ANSI code, but instead a Python query to determine the size of the terminal _at the time this function is called_.  If Python cannot determine this, *DEFAULT_COLS* and *DEFAULT_ROWS* are returned. |
| sufficient_size() | min_cols<br />min_rows | Integer >= 1<br />Integer >= 1 | An enhanced version of size(), this function returns 3 values: True or False if the current screen size meets the specified minimum values, current number of columns, and current number of rows. |
| title() | text<br />window<br />icon<br />alt | Any text<br />True / False<br />True / False<br />True / False | Set the title of the window and/or icon.  If the title is not getting set, try setting alt=True to generate an older version of the ANSI code. |
