# Easy ANSI: attributes module

## Description

The _attributes_ module is a collection of constants to apply effects to the on-screen font.

## Usage

```python
from easyansi.core import attributes
```

## Constants

| Constant | Description |
| --- | --- |
| NORMAL | This sets the brightness of the font to standard (medium, or between DIM and BRIGHT). |
| BRIGHT / BRIGHT_ON<br />BRIGHT_OFF | This sets the the brightness of the font to high, or back to normal. |
| DIM / DIM_ON<br />DIM_OFF | This sets the the brightness of the font to low, or back to normal. |
| BLINK / BLINK_ON<br />BLINK_OFF | Turn blink on or off. |
| CONCEAL / CONCEAL_ON<br />CONCEAL_OFF | Turn conceal on or off.<br />Note that this is **NOT** a secure way to handle passwords, this is just a screen trick where the foreground and background color is the same. |
| ITALIC / ITALIC_ON<br />ITALIC_OFF | Turn italics on or off. |
| REVERSE / REVERSE_ON<br />REVERSE_OFF | Turn reverse (inverse) on or off. |
| STRIKETHROUGH / STRIKETHROUGH_ON<br />STRIKETHROUGH_OFF | Turn strikethrough (cross-out) on or off. |
| UNDERLINE / UNDERLINE_ON<br />UNDERLINE_OFF | Turn underline on or off. |
