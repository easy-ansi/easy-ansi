# Easy ANSI: Standard Colors Module (16-color palette)

## Description

This module gives you full access to the 16-color palette.  All colors can be referenced from constants.  There is a special constant DEFAULT, which will give you the ANSI code for
the terminal default color.

## Usage

```python
from easyansi.core import colors
```

## Color Constants

| Color<br>Index | Index Constant | Foreground Constant | Background Constant |
| --- | --- | --- | --- |
| -1 | DEFAULT_IDX | DEFAULT<br>DEFAULT_FG | DEFAULT_BG |
| 0 | BLACK_IDX | BLACK<br>BLACK_FG | BLACK_BG |
| 1 | RED_IDX | RED<br>RED_FG | RED_BG |
| 2 | GREEN_IDX | GREEN<br>GREEN_FG | GREEN_BG |
| 3 | YELLOW_IDX | YELLOW<br>YELLOW_FG | YELLOW_BG |
| 4 | BLUE_IDX | BLUE<br>BLUE_FG | BLUE_BG |
| 5 | MAGENTA_IDX | MAGENTA<br>MAGENTA_FG | MAGENTA_BG |
| 6 | CYAN_IDX | CYAN<br>CYAN_FG | CYAN_BG |
| 7 | WHITE_IDX | WHITE<br>WHITE_FG | WHITE_BG |
| 8 | BRIGHT_BLACK_IDX | BRIGHT_BLACK<br>BRIGHT_BLACK_FG | BRIGHT_BLACK_BG |
| 9 | BRIGHT_RED_IDX | BRIGHT_RED<br>BRIGHT_RED_FG | BRIGHT_RED_BG |
| 10 | BRIGHT_GREEN_IDX | BRIGHT_GREEN<br>BRIGHT_GREEN_FG | BRIGHT_GREEN_BG |
| 11 | BRIGHT_YELLOW_IDX | BRIGHT_YELLOW<br>BRIGHT_YELLOW_FG | BRIGHT_YELLOW_BG |
| 12 | BRIGHT_BLUE_IDX | BRIGHT_BLUE<br>BRIGHT_BLUE_FG | BRIGHT_BLUE_BG |
| 13 | BRIGHT_MAGENTA_IDX | BRIGHT_MAGENTA<br>BRIGHT_MAGENTA_FG | BRIGHT_MAGENTA_BG |
| 14 | BRIGHT_CYAN_IDX | BRIGHT_CYAN<br>BRIGHT_CYAN_FG | BRIGHT_CYAN_BG |
| 15 | BRIGHT_WHITE_IDX | BRIGHT_WHITE<br>BRIGHT_WHITE_FG | BRIGHT_WHITE_BG |
