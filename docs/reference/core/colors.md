# Easy ANSI: General Colors Information

## Do Not Mix Color Palettes

As a general rule, you should not mix colors from different color palettes (16, 256, and RGB) on the same screen.
I have had mixed results in testing this, and depending on the terminal emulator, it is not always handled gracefully.

## Color Constants Pattern

Color constants are named with a pattern, as follows (using BLUE as an example):

| Constant Example | Description |
| --- | --- |
| BLUE_IDX | This is an index number for the color, which can be passed to the ColorBuilder.color() function. |
| BLUE_FG | This is a pre-generated color code for the foreground color.  You can place this in your strings without generating a code. |
| BLUE | This is the same as BLUE_FG.  It is provided to give you less typing in your code. |
| BLUE_BG | This is a pre-generated color code for the background color.  You can place this in your strings without generating a code. |

## The First 16 Colors

All the colors in the 16-color palette, and the first 16 colors in the 256-color palette, are defined by your
terminal emulator.  The default text color is also user or theme defined.  You cannot assume that the colors you think
you are programming for are going to be the colors that get displayed on another user's terminal.  If you need exact
colors that should not be changed, you should use the RGB color palette.

Below are some screenshots showing that the theming is fully customizable by the user, which is outside of your control:

| Description | Image |
| --- | --- |
| KDE Konsole | ![KDE Profile Manager](../images/KDE_Profile_Manager.png "KDE Profile Manager")<br>![KDE Color Scheme](../images/KDE_Color_Scheme.png "KDE Color Scheme") |
| GNOME Terminal | ![GNOME Color Preferences](../images/GNOME_Color_Preferences.png "GNOME Color Preferences") |
