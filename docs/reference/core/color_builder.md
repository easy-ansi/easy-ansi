# Easy ANSI: The Color Builder

## Overview

**TL;DR:** If you do not wish to learn how to use the color builder, you can use the built-in colors module
(easyansi.core.colors) and use the 16 pre-built standard colors.

The Easy ANSI Color Builder is an Enum class that will build ANSI color codes for the palette mode you choose.
It has 1 public method, color(), which is very flexible and handles everything for you.

## Quick Reference Table

| Item | 16-Color Palette | 256-Color Palette | RGB-Color Palette |
| --- | --- | --- | --- |
| Enum | ColorBuilder.PALETTE_16 | ColorBuilder.PALETTE_256 | ColorBuilder.PALETTE_RGB |
| Single Integer Range | 0 - 15 | 0 - 255 | 0 - 16777215 |
| Sequence Integer Range | 0 - 15 | 0 - 255 | 0 - 255, 0 - 255, 0 - 255 |
| Single Hex Range | #0 - #f | #00 - #ff | #000000 - #ffffff |
| Sequence Hex Range | #0 - #f | #00 - #ff | #00 - #ff, #00 - #ff, #00 - #ff |
| Index For Default Color | -1 | -1 | -1 |

## Usage: Create ColorBuilder Instance

```python
from easyansi.core.color_builder import ColorBuilder

# Create a ColorBuilder for the color palette you wish to use
cb_16 = ColorBuilder.PALETTE_16
cb_256 = ColorBuilder.PALETTE_256
cb_rgb = ColorBuilder.PALETTE_RGB
```

## Usage: Creating Colors (In General)

When creating colors, you can specify a value for foreground, background, or both.  Color codes are
different based on whether they are for the foreground or background.  To not get a specific color value, leave
it at a value of *None*.

Regardless of the color palette you are using, you can pass a color value of *-1* to get the terminal default
color.  Remember that there is a default for both the foreground and background, so if you want a full color
reset, you need to request both codes.

Also note that it is safe to mix and match the types of values you pass in to the color() function.  Each type
is described below.

To see all the available colors, see the Easy ANSI color demos [here](https://gitlab.com/easy-ansi/easy-ansi-demos).
You can also look at a Wikipedia entry about ANSI colors 
[here](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit).

Below are examples using the 16-color palette:

```python
# Gets a foreground color of 1 (red)
color_code = cb_16.color(1)
color_code = cb_16.color(1, None)
color_code = cb_16.color(fg=1, bg=None)

# Gets a background color of 2 (green)
color_code = cb_16.color(, 2)
color_code = cb_16.color(None, 2)
color_code = cb_16.color(bg=2)
color_code = cb_16.color(fg=None, bg=2)

# Gets both a foreground color of 7 (white) and a background color of 4 (blue)
color_code = cb_16.color(7, 4)
color_code = cb_16.color(fg=7, bg=4)

# Get the default colors as defined by the terminal
color_code = cb_16.color(-1, -1)
```

## Usage: Create A Color With Integers

All colors are integers behind the scenes.  The color palette being used determines what color each value represents.

For RGB values, you are not likely to use this method of retrieving a color, but instead you will more likely use
hex codes or sequences (see below).

```python
# 16-color palette
color_code = cb_16.color(1)
color_code = cb_16.color(, 2)
color_code = cb_16.color(1, 2)

# 256-color palette
color_code = cb_256.color(100)
color_code = cb_256.color(, 200)
color_code = cb_256.color(100, 200)

# RGB-color palette
color_code = cb_rgb.color(1000000)
color_code = cb_rgb.color(, 2000000)
color_code = cb_rgb.color(1000000, 2000000)
```

## Usage: Create A Color With Hex Values

Web developers will be familiar with this method of creating a color.  Hex codes can be created using 2 different
syntaxes (case insensitive):

* 0xab : Python will interpret this as an integer and treat it as such
* "#ab", "ab", "0xab" : Strings are **ALWAYS** treated as hex values
* "#aabbcc", "aabbcc", "0xaabbcc" : For the RGB color palette, the 6 character hex code is treated just like CSS, in that aa=red value, bb=green value, cc=blue value.

```python
# 16-color palette
color_code = cb_16.color(0xa, 0xB)
color_code = cb_16.color("a", "B")
color_code = cb_16.color("#a", "#B")

# 256-color palette
color_code = cb_256.color(0xaa, 0xBB)
color_code = cb_256.color("aa", "BB")
color_code = cb_256.color("#aa", "#BB")

# RGB-color palette
color_code = cb_rgb.color(0xaabbcc, 0xAABBCC)
color_code = cb_rgb.color("aabbcc", "AABBCC")
color_code = cb_rgb.color("#aabbcc", "#AABBCC")
```

## Usage: Create A Color With Sequences

Another way you can request a color value is with a list or tuple, and this will be familiar with web developers.
While this is most useful for the RGB color palette, it is possible to use with the other color palettes.

```python
# RGB-color palette
color_code = cb_rgb.color(("aa", "#bb", "0xcc"), ("11", "#22", "0x33"))
color_code = cb_rgb.color((0xaa, 0xbb, 0xcc), (0x11, 0x22, 0x33))
color_code = cb_rgb.color([170, 187, 204], [17, 34, 51])

# 16-color palette
color_code = cb_16.color(("a",), ("#b",))
color_code = cb_16.color((0xa,), (0xb,))
color_code = cb_16.color([10,], [11,])

# 256-color palette
color_code = cb_16.color(("aa",), ("#bb",))
color_code = cb_16.color((0xaa,), (0xbb,))
color_code = cb_16.color([170,], [187,])

```

## Properties

| Property | Description |
| --- | --- |
| min_value | The minimum color value. |
| max_value | The maximum color value. |
| max_value_int | For sequences, the maximum value for a specific value. |
| default_value | The index used to get the default value. |
| palette | The total number of colors available. |

## Constants

The constants are defined under the color_builder module (easyansi.core.color_builder).

| Constant | Description |
| --- | --- |
| DEFAULT_BG | The ANSI color code for the default background. |
| DEFAULT_FG<br />DEFAULT | The ANSI color code for the default foreground. |
| DEFAULT_IDX | The index number used to retrieve the default color values. |
| DEFAULT_IDX_STR | The string version of DEFAULT_IDX. |
| MIN_VALUE | The minimum color value. |
