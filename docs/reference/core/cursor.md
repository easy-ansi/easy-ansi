# Easy ANSI: cursor module

## Description

The _cursor_ module is a collection of constants and functions related to movement, showing/hiding, and getting
the position of the cursor.

**Note:** The ANSI standard accepts coordinates in a row, col (y, x).  This has been converted for you, in
that you can use normal geometrical col, row (x, y) coordinates.

**Note:** The ANSI standard accepts coordinates in a 1-based system (ie, the top-left corner is 1, 1).  This has
been converted for you, in that you can use a 0-based coordinate system (ie, the top-left corner is 0, 0).

## Usage

```python
from easyansi.core import cursor
```

## Constants

| Constant | Description |
| --- | --- |
| DOWN | Move the cursor down 1 row. |
| HIDE / HIDE_ON / SHOW_OFF | Hide the cursor. |
| SHOW / SHOW_ON / HIDE_OFF | Show the cursor. |
| HOME / HOME_ON_SCREEN | Move the cursor to the top left corner of the screen (0, 0). |
| HOME_ON_ROW | Move the cursor to column 0 on the current row. |
| LEFT | Move the cursor left 1 column. |
| MIN_COL | The minimum column number. |
| MIN_MOVEMENT | The minimum amount of movement you can request the cursor to move. |
| MIN_ROW | The minimum row number. |
| NEXT_LINE | Move the cursor to column 0 of the next row. |
| PREVIOUS_LINE | Move the cursor to column 0 of the previous row. |
| RIGHT | Move the cursor right 1 column. |
| UP | Move the cursor up 1 row. |


## Functions

| Function | Parameters | Valid Values | Default | Description |
| --- | --- | --- | --- | --- |
| down() | rows | Integer > 0 | 1 | Move the cursor down a certain number of rows. |
| left() | cols | Integer > 0 | 1 | Move the cursor left a certain number of columns. |
| locate() | col<br />row | Integer >= 0<br />Integer >= 0 | n/a<br />n/a | Move the cursor to (column, row). |
| locate_col() | col | Integer >= 0 | n/a | Move the cursor to the indicated column on the current row. |
| move() | cols<br />rows | Integer != 0 for one or both values | None<br />None | Move the cursor relative to its current position.</br>Columns > 0 = move right, else move left<br />Rows > 0 = move down, else move up |
| next_line() | rows | Integer > 0 | 1 | Move the cursor down a certain number of rows, and to column 0. |
| position() | n/a | n/a | n/a | Returns the (column, row) of the current cursor position. |
| previous_line() | rows | Integer > 0 | 1 | Move the cursor up a certain number of rows, and to column 0. |
| right() | cols | Integer > 0 | 1 | Move the cursor right a certain number of columns. |
| up() | rows | Integer > 0 | 1 | Move the cursor up a certain number of rows. |
