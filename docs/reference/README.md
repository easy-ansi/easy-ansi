# Easy ANSI

## Core Documentation

* [General Color Information](./core/colors.md)
  * [The Standard 16 Colors](./core/standard_colors.md)
  * [The Color Builder](./core/color_builder.md)
* [Attributes](./core/attributes.md)
* [Screen](./core/screen.md)
* [Cursor](./core/cursor.md)  
* [Utils](./core/utils.md)
* [Core](./core/core.md)

## Drawing Documentation

* [Drawing](./drawing/general.md)
  * [Line Characters](./drawing/line_chars.md)
  * [Lines](./drawing/lines.md)
  * [Shapes](./drawing/shapes.md)

## Acronyms / Glossary

| Acronym / Term | Description |
| --- | --- |
| ANSI | American National Standards Institute |
| Console<br>Terminal | This is the software (or screen mode) used for text + ANSI output.  These terms are used interchangeably, as far as this project is concerned. |
| CSI | Control Sequence Introducer |
| Easy | Not hard. |
| TUI | Text User Interface |

## Abbreviations

| Abbreviation | Description |
| --- | --- |
| BG | Background |
| BWD | Backward |
| COL / COLS | Column(s) |
| ESC | Escape |
| FG | Foreground |
| FWD | Forward |
| IDX | Index |
| LEN | Length |
| LN | Line |
| MIN | Minimum |
| PREV | Previous |
| PRNT | Print |
| STR | String |
